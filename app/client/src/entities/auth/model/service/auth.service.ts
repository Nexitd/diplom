import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'shared/config';
import { LoginDataType, LoginResponseType } from 'shared/types';

export const authApi = createApi({
    reducerPath: 'authApi',
    baseQuery: fetchBaseQuery({ baseUrl: baseUrl }),
    endpoints: (build) => ({
        login: build.mutation<LoginResponseType, LoginDataType>({
            query: (data) => ({
                url: 'auth/login',
                method: 'POST',
                body: data,
            }),

            transformResponse: (response: LoginResponseType) => response,
        }),
    }),
});

export const { useLoginMutation } = authApi;
