import { parseJwt } from "shared/api"
import { sidebarData } from "../config"
import { SidebarItem } from "./sidebar-item"

const Sidebar = () => {
    const { role } = parseJwt(localStorage.getItem("access_token"))

    return <div className="sidebar">
        {role && sidebarData[role].map(el => (
            <SidebarItem key={el.id} data={el} />
        ))}
    </div>
}

export default Sidebar