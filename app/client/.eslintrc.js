/** Разрешенные импорты (с публичными API) */
const ALLOWED_PATH_GROUPS = [
    'pages/**',
    'widgets/**',
    'features/**',
    'entities/**',
    'shared/**',
].map((pattern) => ({
    pattern,
    group: 'internal',
    position: 'after',
}));
/** Для запрета приватных путей */
const DENIED_PATH_GROUPS = [
    // Private imports are prohibited, use public imports instead
    'app/**',
    'pages/*/**',
    'widgets/*/**',
    'features/*/**',
    'entities/*/**',
    'shared/*/*/**',
    // Prefer absolute imports instead of relatives (for root modules)
    '../**/app',
    '../**/pages',
    '../**/widgets',
    '../**/features',
    '../**/entities',
    '../**/shared',
];

module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 2020,
        ecmaFeatures: {
            jsx: true,
            modules: true,
        },
        sourceType: 'module',
    },
    env: {
        browser: true,
        es6: true,
    },
    plugins: ['react', '@typescript-eslint', 'unicorn'],
    extends: [
        'react-app',
        'eslint:recommended',
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
        'plugin:react/recommended',
        'plugin:@typescript-eslint/recommended',
        'prettier',
    ],
    rules: {
        // imports
        'import/first': 2,
        'import/no-unresolved': 0,
        'import/order': [
            2,
            {
                pathGroups: ALLOWED_PATH_GROUPS,
                pathGroupsExcludedImportTypes: ['builtin'],
                groups: ['builtin', 'external', 'internal', 'parent', 'sibling', 'index'],
            },
        ],
        'no-restricted-imports': [2, { patterns: DENIED_PATH_GROUPS }],
        // variables
        'prefer-const': 2,
        'no-var': 2,
        'no-unused-vars': 'off',
        // base
        camelcase: [
            1,
            {
                ignoreDestructuring: true,
                ignoreImports: true,
                properties: 'never',
            },
        ],
        'no-else-return': 2,
        'dot-notation': 2,
        'eol-last': 0,
        // alert, console
        'no-alert': 2,
        'no-console': 'off',
        'no-debugger': 'off',
        // equals
        eqeqeq: 1,
        'no-eq-null': 2,
        // function
        'max-params': [1, 2],
        'arrow-parens': [1, 'as-needed'],
        // "arrow-body-style": [1, "as-needed"],
        // plugin:unicorn
        'unicorn/no-for-loop': 2,
        'unicorn/no-abusive-eslint-disable': 2,
        'unicorn/no-array-instanceof': 2,
        'unicorn/no-zero-fractions': 2,
        'unicorn/prefer-includes': 2,
        'unicorn/prefer-text-content': 2,
        'unicorn/import-index': 2,
        'unicorn/throw-new-error': 2,
        // plugin: react
        'react/display-name': [0, { ignoreTranspilerName: 0, checkContextObjects: 0 }],
        'react/jsx-uses-react': 0,
        'react/react-in-jsx-scope': 0,
        'react/prop-types': 0,
        'no-restricted-globals': 1,
    },
    overrides: [
        {
            files: ['*.ts'],
            rules: {
                'no-undef': 'off',
            },
        },
    ],
};
