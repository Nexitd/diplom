import { ComponentType } from "react"
import { roles } from "shared/types";

export type PrivateRouteType = {
    path: string,
    roles: roles[];
    isPublic: boolean;
}

export type RouteType = {
    element: ComponentType
}