"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImagesService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../prisma/prisma.service");
const path = require("path");
const fs = require("fs");
let ImagesService = class ImagesService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async addFile(file, sendedData) {
        const queryData = Object.assign({}, Object.keys(sendedData).map((key) => {
            if (sendedData[key]) {
                return { [key]: Number(sendedData[key]) };
            }
            else
                return { [key]: sendedData[key] };
        })[0]);
        const createdFile = await this.prisma.images.create({
            data: Object.assign(Object.assign({}, queryData), { name: file.filename, link: `/${file.path.replace(/\\/g, '/')}` }),
        });
        return createdFile;
    }
    async findAll() {
        const files = await this.prisma.images.findMany();
        return files;
    }
    async remove(id) {
        const image = await this.prisma.images.findUnique({
            where: {
                id: id,
            },
        });
        if (image) {
            await this.prisma.images.delete({
                where: {
                    id: id,
                },
            });
            fs.unlinkSync(path.resolve('./' + image.link));
            return { id: image.id, message: 'Successfully removed' };
        }
        throw new common_1.NotFoundException('image.not_found');
    }
};
ImagesService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], ImagesService);
exports.ImagesService = ImagesService;
//# sourceMappingURL=images.service.js.map