import React, { ReactNode } from "react";
import cn from "classnames"


type Props = {
    text: string;
    isImage?: boolean;
    onClick?: () => void;
    className?: string;
    type?: React.ButtonHTMLAttributes<HTMLButtonElement>["type"];
    icon?: ReactNode;
}

const Button = (props: Props) => {
    const {
        text = '',
        isImage = false,
        className = '',
        onClick = () => ({}),
        type = "button",
        icon = <></>
    } = props;

    return (
        <>
            {!isImage ?
                <button
                    className={cn('btn', className)}
                    type={type}
                    onClick={onClick}
                >
                    {text}
                </button>
                :
                <button
                    type={type}
                    onClick={onClick}
                    className={cn('btn', className)}
                >
                    <span>{icon}</span>
                    {text}
                </button>
            }
        </>
    )
}

export default Button;