import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { PrismaModule } from './prisma/prisma.module';
import { RealEstatesModule } from './real_estates/real_estates.module';
import { DataGeneratorModule } from './generators/data-generator.module';
import { ImagesModule } from './images/images.module';
import { CompaniesModule } from './companies/companies.module';
import { AppController } from './app.controller';
import { DealsModule } from './deals/deals.module';
import * as passport from 'passport';
import * as session from 'express-session';

@Module({
  controllers: [AppController],
  imports: [
    AuthModule,
    UsersModule,
    RealEstatesModule,
    PrismaModule,
    DataGeneratorModule,
    ImagesModule,
    CompaniesModule,
    MulterModule.registerAsync({
      useFactory: () => ({
        dest: './uploads',
      }),
    }),
    DealsModule,
  ],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        session({
          name: 'sessionId',
          proxy: true,
          rolling: true,
          resave: false,
          saveUninitialized: false,
          secret: process.env.JWT_SECRET,
          cookie: {
            sameSite: false,
            secure: false,
            maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days
          },
        }),
        passport.initialize(),
        passport.session(),
      )
      .forRoutes('*');
  }
}
