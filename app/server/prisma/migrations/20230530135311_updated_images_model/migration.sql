-- DropForeignKey
ALTER TABLE "images" DROP CONSTRAINT "images_company_id_fkey";

-- DropForeignKey
ALTER TABLE "images" DROP CONSTRAINT "images_real_estate_id_fkey";

-- DropForeignKey
ALTER TABLE "images" DROP CONSTRAINT "images_user_id_fkey";

-- AlterTable
ALTER TABLE "images" ALTER COLUMN "real_estate_id" DROP NOT NULL,
ALTER COLUMN "real_estate_id" SET DEFAULT 0,
ALTER COLUMN "user_id" DROP NOT NULL,
ALTER COLUMN "user_id" SET DEFAULT 0,
ALTER COLUMN "company_id" DROP NOT NULL,
ALTER COLUMN "company_id" SET DEFAULT 0;

-- AddForeignKey
ALTER TABLE "images" ADD CONSTRAINT "images_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "images" ADD CONSTRAINT "images_real_estate_id_fkey" FOREIGN KEY ("real_estate_id") REFERENCES "real_estate"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "images" ADD CONSTRAINT "images_company_id_fkey" FOREIGN KEY ("company_id") REFERENCES "company"("id") ON DELETE SET NULL ON UPDATE CASCADE;
