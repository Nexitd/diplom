import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './services/auth.service';
import { AuthController } from './controllers/auth.controller';
import { PrismaService } from 'src/prisma/prisma.service';
import { LocalStrategy } from './strategy/local.strategy';
import { AuthSerializer } from './serialize/serialize.provider';
import { JwtStrategy } from './strategy/jwt.strategy';

@Module({
  imports: [
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: '7d' },
    }),
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    PrismaService,
    AuthSerializer,
    LocalStrategy,
    JwtStrategy,
  ],
})
export class AuthModule {}
