import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsPositive,
  IsString,
  Max,
  Min,
  MaxLength,
  MinLength,
  IsPhoneNumber,
} from 'class-validator';

export class CreateUserDto {
  @MinLength(5)
  @IsNotEmpty()
  @IsString()
  name: string;

  @MinLength(5)
  @IsNotEmpty()
  @IsString()
  surname: string;

  @MinLength(5)
  @IsNotEmpty()
  @IsString()
  patronymic: string;

  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;

  @IsPhoneNumber('RU')
  phone: string;

  @MinLength(5)
  @MaxLength(15)
  @IsNotEmpty()
  @IsString()
  password: string;
}
