/// <reference types="multer" />
import { NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { images as ImageModel } from '@prisma/client';
export declare class ImagesService {
    private readonly prisma;
    constructor(prisma: PrismaService);
    addFile(file: Express.Multer.File, sendedData: {
        user_id: number;
    } | {
        company_id: number;
    } | {
        real_estate_id: number;
    }): Promise<ImageModel>;
    findAll(): Promise<ImageModel[]>;
    remove(id: number): Promise<{
        id: number;
        message: 'Successfully removed';
    } | NotFoundException>;
}
