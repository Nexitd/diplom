import { CanActivate, ExecutionContext } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
export declare class LoggedInGuard implements CanActivate {
    protected readonly reflector?: Reflector;
    constructor(reflector?: Reflector);
    canActivate(context: ExecutionContext): any;
}
