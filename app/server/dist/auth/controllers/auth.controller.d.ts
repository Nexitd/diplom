import { Response } from 'express';
import { AuthService } from '../services/auth.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { LoginUserDto } from 'src/users/dto/login-user.dto';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    registerUser(user: CreateUserDto): Promise<import(".prisma/client").user>;
    logInUser(user: LoginUserDto, res: Response): Promise<void>;
}
