import about from "assets/images/about/about.png"
import { MainFeedbackIcon, MainHouseIcon, MainProfIcon } from "shared/icons"

const MainPageAbout = () => {
    return <div className="main__page_about about">
        <h2 className="title-font about__title">О платформе</h2>

        <div className="about__container">
            <div className="about__container_item">
                <img src={about} alt="about pic" />

                <div className="about__block"></div>
            </div>
            <div className="about__container_item">
                <h3 className="about__subtitle">Как это работает?</h3>
                <p className="about__text">
                    На платформе «Lorem Ipsum» Вы сможете найти выгодные предложения о покупке недвижимости. Всё, что вам нужно сделать — это подобрать подходящий для себя объект и выбрать риелтора из предложенного списка. Всю остальную работу он выполнит сам, Вам останется только обсудить необходимые вопросы с юристом, внести оплату за жильё и получить ключи!
                    Все объекты, которые Вам понравились, Вы можете добавить в Избранное и потом найти их в своём личном кабинете в соответствующем разделе.
                </p>

                <div className="about__icons">
                    <div className="about__icons_item">
                        <MainHouseIcon />
                        <span className="about__icons_text">Проверенное жильё</span>
                    </div>
                    <div className="about__icons_item">
                        <MainFeedbackIcon />
                        <span className="about__icons_text">Качественный сервис</span>
                    </div>
                    <div className="about__icons_item">
                        <MainProfIcon />
                        <span className="about__icons_text">Профессиональная работа</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default MainPageAbout