import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/dist/query/react';
import { baseUrl } from 'shared/config';

export const realEstateApi = createApi({
    reducerPath: 'realEstateApi',
    baseQuery: fetchBaseQuery({ baseUrl: baseUrl, credentials: 'include' }),
    tagTypes: ['Real-Estate'],
    endpoints: (build) => ({
        addRealEstate: build.mutation<any, any>({
            query: (data) => ({
                url: 'real-estates',
                method: 'POST',
                body: data,
            }),
        }),
    }),
});

export const {useAddRealEstateMutation} = realEstateApi