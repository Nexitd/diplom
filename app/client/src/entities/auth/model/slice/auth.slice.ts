import { PayloadAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios, { AxiosResponse } from 'axios';
import { baseUrl, token } from 'shared/config';
import { LoginResponseType, UserType } from 'shared/types';

type initialStateType = {
    isAuth: boolean;
    isError: boolean;
    isPending: boolean;
    errorStatus: string;
    isSuccess: boolean;
};

export const Login: any = createAsyncThunk(
    'auth/Login',
    async (values: { email: string; password: string }) => {
        try {
            const { data }: AxiosResponse = await axios.post(`${baseUrl}auth/login`, values, {
                withCredentials: true,
            });

            return data;
        } catch (e: any) {
            const err = e as any;

            console.log(err);

            throw new Error(err.response.data.message);
        }
    },
);

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        isAuth: !!localStorage.getItem('access_token'),
        isError: false,
        errorStatus: '',
        isSuccess: false,
        isPending: false,
    } as initialStateType,
    reducers: {
        logOut: (state) => {
            state.isAuth = false;
            state.isSuccess = false;
            state.isPending = false;
            state.isError = false;
            state.errorStatus = '';

            localStorage.removeItem('access_token');
        },
    },
    extraReducers: {
        [Login.fulfilled]: (state, { payload }: PayloadAction<LoginResponseType>) => {
            state.isAuth = true;
            state.isError = false;
            state.isPending = false;
            state.isSuccess = true;

            localStorage.setItem('access_token', payload.token);
        },
        [Login.pending]: (state) => {
            state.isSuccess = false;
            state.isAuth = false;
            state.isPending = true;
            state.isError = false;
        },
        [Login.rejected]: (state, { error }) => {
            state.isAuth = false;
            state.isSuccess = false;
            state.isPending = false;
            state.isError = true;
            state.errorStatus = error.message;
        },
    },
});

export const { logOut } = authSlice.actions;

export default authSlice.reducer;
