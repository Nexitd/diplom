import { OnModuleInit } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
export declare class RolesGenerator implements OnModuleInit {
    private readonly prisma;
    private logger;
    constructor(prisma: PrismaService);
    generateRoles(): Promise<void>;
    onModuleInit(): Promise<void>;
}
