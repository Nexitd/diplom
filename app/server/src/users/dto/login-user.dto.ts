import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class LoginUserDto {
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;

  @MinLength(5)
  @MaxLength(15)
  @IsNotEmpty()
  @IsString()
  password: string;
}
