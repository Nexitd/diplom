import { NavLink, useLocation } from 'react-router-dom';
import cn from 'classnames';
import logo2 from "assets/images/лого 2.svg"
import logo1 from "assets/images/Logo 1.svg"
import { UserModel } from 'entities/user';
import { AuthModel } from 'entities/auth';
import { parseJwt, useAppDispatch, useAppSelector } from 'shared/api';
import { Button } from 'shared/ui';
import { actions } from 'shared/constants';
import { AccountIcon, LogoutBtn } from 'shared/icons';
import { uploadFilesUrl } from 'shared/config';


type HeaderProps = {
    className?: string;
}

const Header = ({ className = "" }: HeaderProps) => {
    const { pathname } = useLocation();
    const { isAuth } = useAppSelector(state => state.auth);
    const dispatch = useAppDispatch()
    const { id } = parseJwt(localStorage.getItem("access_token"))
    const { data } = UserModel.useGetMeQuery(id!)

    return (
        <header className={cn('header', className)}>
            <div className='header__container'>
                <div className='header__container_links'>
                    <NavLink to="/" className='header__container_logolink'>
                        <img src={pathname === "/" ? logo1 : logo2} alt="logo" />
                    </NavLink>


                    {actions.map(({ id, label, url }) => (
                        <NavLink
                            key={id}
                            to={url}
                            className='header__container_link'
                        >
                            <span>{label}</span>
                        </NavLink>
                    ))}
                </div>

                {!isAuth ?
                    <Button type="button" text="Войти" className='header__container_btn' />
                    : <div className='header__container_footer'>
                        <button className="header__logout_btn" onClick={() => dispatch(AuthModel.logOut())}>
                            <LogoutBtn />
                        </button>
                        <NavLink className="header__container_logolink" to="/personal-area">
                            {data?.images !== null ?
                                <img className='header__profile' src={`${uploadFilesUrl}${data?.images.link}`} alt="" />
                                : <AccountIcon />}
                        </NavLink>
                    </div>
                }
            </div>
        </header>
    )
}

export default Header;