import { UsersService } from '../services/users.service';
import { UpdateUserDto } from '../dto/update-user.dto';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    findAll(): Promise<Omit<import(".prisma/client").user, "password">[]>;
    findOne(id: string): Promise<Omit<import(".prisma/client").user, "password">>;
    update(id: string, updateUserDto: UpdateUserDto): Promise<void>;
    remove(id: string): Promise<void>;
}
