import { Controller, Get, Param, Res } from '@nestjs/common';
import { Response } from 'express';

@Controller()
export class AppController {
  constructor() {}

  @Get('uploads/:path')
  seeUploadedFile(@Param('path') image: string, @Res() res: Response) {
    return res.sendFile(image, { root: './uploads' });
  }
}
