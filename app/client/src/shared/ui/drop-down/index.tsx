import { Select } from 'antd';
import cn from 'classnames';
import { selectItemsType } from 'shared/types';

type DropDownProps = {
    items: selectItemsType[];
    title: string;
    searchParam: string;
    className?: string;
    onChange?: (value: string, searchParam: string) => void
}

const DropDown = ({ title = "hover me", items, className = "", searchParam, onChange = () => undefined }: DropDownProps) => {
    return <Select
        className={cn('dropdown', className)}
        defaultValue={title}
        onChange={(value) => onChange(value, searchParam)}
        options={items}
    />
}

export default DropDown;