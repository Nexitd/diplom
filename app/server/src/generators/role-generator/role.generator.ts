import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { roles } from 'src/common/constants/roles';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class RolesGenerator implements OnModuleInit {
  private logger = new Logger();
  constructor(private readonly prisma: PrismaService) {}
  async generateRoles() {
    for (let role of roles) {
      const role_ = await this.prisma.role.findUnique({
        where: {
          name: role,
        },
      });

      if (!role_) {
        await this.prisma.role.create({
          data: {
            name: role,
          },
        });
      }
    }
  }

  async onModuleInit() {
    this.logger.log('Roles generating starting!');
    await this.generateRoles();
    this.logger.log('Roles generating end!');
  }
}
