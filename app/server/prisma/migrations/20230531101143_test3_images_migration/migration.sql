/*
  Warnings:

  - A unique constraint covering the columns `[real_estate_id]` on the table `images` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "images_real_estate_id_key" ON "images"("real_estate_id");
