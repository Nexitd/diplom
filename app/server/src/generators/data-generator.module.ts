import { Module } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { RolesGenerator } from './role-generator/role.generator';

@Module({
  providers: [RolesGenerator, PrismaService],
})

export class DataGeneratorModule {}
