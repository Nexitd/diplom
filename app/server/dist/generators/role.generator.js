"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesGenerator = void 0;
const common_1 = require("@nestjs/common");
const roles_1 = require("../common/constants/roles");
const prisma_service_1 = require("../prisma/prisma.service");
let RolesGenerator = class RolesGenerator {
    constructor(prisma) {
        this.prisma = prisma;
        this.logger = new common_1.Logger();
    }
    async generateRoles() {
        for (let role of roles_1.roles) {
            const role_ = await this.prisma.role.findUnique({
                where: {
                    name: role,
                },
            });
            if (!role_) {
                await this.prisma.role.create({
                    data: {
                        name: role,
                    },
                });
            }
        }
    }
    async onModuleInit() {
        this.logger.log('Roles generating starting!');
        this.logger.log('Roles generating end!');
    }
};
RolesGenerator = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], RolesGenerator);
exports.RolesGenerator = RolesGenerator;
//# sourceMappingURL=role.generator.js.map