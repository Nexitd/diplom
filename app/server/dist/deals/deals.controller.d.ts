import { DealsService } from './deals.service';
import { CreateDealDto } from './dto/create-deal.dto';
import { UpdateDealDto } from './dto/update-deal.dto';
export declare class DealsController {
    private readonly dealsService;
    constructor(dealsService: DealsService);
    create(createDealDto: CreateDealDto): string;
    findAll(): string;
    findOne(id: string): string;
    update(id: string, updateDealDto: UpdateDealDto): string;
    remove(id: string): string;
}
