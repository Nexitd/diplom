import {
  Controller,
  Get,
  Post,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  UseGuards,
  Body,
  Query,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { AuthGuard } from '@nestjs/passport';

import { imageFileFilter, storage } from 'src/common/utils/storage';
import { ImagesService } from '../services/images.service';

@Controller('images')
export class ImagesController {
  constructor(private readonly imagesService: ImagesService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(
    FileInterceptor('file', {
      ...storage,
      fileFilter: imageFileFilter,
    }),
  )
  create(
    @UploadedFile() file: Express.Multer.File,
    @Query()
    data: {
      user_id: null | number;
      company_id: null | number;
      real_estate_id: null | number;
    },
  ) {
    return this.imagesService.addFile(file, data);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  findAll() {
    return this.imagesService.findAll();
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  remove(@Param('id') id: string) {
    return this.imagesService.remove(Number(id));
  }
}
