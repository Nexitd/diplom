import { PrismaService } from 'src/prisma/prisma.service';
import { CreateRealEstateDto } from '../dto/create-real_estate.dto';
import { UpdateRealEstateDto } from '../dto/update-real_estate.dto';
import { real_estate } from '@prisma/client';
export declare class RealEstatesService {
    private readonly prisma;
    constructor(prisma: PrismaService);
    create(realEstate: CreateRealEstateDto): Promise<real_estate>;
    findAllObjects(): Promise<real_estate[]>;
    findOne(id: number): Promise<real_estate | {}>;
    update(id: number, updateRealEstateDto: UpdateRealEstateDto): string;
    remove(id: number): Promise<any>;
}
