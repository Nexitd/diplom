declare type RoleType = {
    id: number;
    name: string;
};
declare type UserRolesType = {
    role: RoleType;
};
export declare class UserDto {
    id: number;
    email: string;
    user_roles: UserRolesType[];
}
export {};
