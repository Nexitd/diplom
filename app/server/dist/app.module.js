"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const auth_module_1 = require("./auth/auth.module");
const users_module_1 = require("./users/users.module");
const prisma_module_1 = require("./prisma/prisma.module");
const real_estates_module_1 = require("./real_estates/real_estates.module");
const data_generator_module_1 = require("./generators/data-generator.module");
const images_module_1 = require("./images/images.module");
const companies_module_1 = require("./companies/companies.module");
const app_controller_1 = require("./app.controller");
const deals_module_1 = require("./deals/deals.module");
const passport = require("passport");
const session = require("express-session");
let AppModule = class AppModule {
    configure(consumer) {
        consumer
            .apply(session({
            name: 'sessionId',
            proxy: true,
            rolling: true,
            resave: false,
            saveUninitialized: false,
            secret: process.env.JWT_SECRET,
            cookie: {
                sameSite: false,
                secure: false,
                maxAge: 1000 * 60 * 60 * 24 * 30,
            },
        }), passport.initialize(), passport.session())
            .forRoutes('*');
    }
};
AppModule = __decorate([
    (0, common_1.Module)({
        controllers: [app_controller_1.AppController],
        imports: [
            auth_module_1.AuthModule,
            users_module_1.UsersModule,
            real_estates_module_1.RealEstatesModule,
            prisma_module_1.PrismaModule,
            data_generator_module_1.DataGeneratorModule,
            images_module_1.ImagesModule,
            companies_module_1.CompaniesModule,
            platform_express_1.MulterModule.registerAsync({
                useFactory: () => ({
                    dest: './uploads',
                }),
            }),
            deals_module_1.DealsModule,
        ],
        providers: [],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map