import { useEffect, useState } from "react"
import { MainFilters } from "features/main-page-filters";
import { mainPageSliderImages } from "../config"

const useSliderControlsLogic = (currSlide = 0) => {
    const selectedSlide = mainPageSliderImages.filter(el => el.id === currSlide);

    return selectedSlide[0].image;
}


const MainPageCarousel = () => {
    const [currSlide, setCurrSlide] = useState(0);
    const image = useSliderControlsLogic(currSlide)

    const goToNextSlide = () => {
        if (currSlide === mainPageSliderImages.length - 1) {
            setCurrSlide(0);

            return
        }

        setCurrSlide(prev => prev + 1)
    }

    const goToPrevSlide = () => {
        if (currSlide <= 0) {
            setCurrSlide(mainPageSliderImages.length - 1)

            return
        }

        setCurrSlide(prev => prev - 1)
    }

    useEffect(() => {
        const carouselInterval = setInterval(goToNextSlide, 2500)

        return () => clearInterval(carouselInterval);
    }, [currSlide])

    return <div className='main__slider' style={{
        backgroundImage: `url(${image})`
    }}>
        <div className='main__slider_container'>
            <div className='main__slider_item'>
                <div className='item_mt'>
                    <h1 className='main__slider_title title-font'>Покупка и аренда недвижимости</h1>
                    <p className='main__slider_text'>Подберите такую недвижимость, которая будет отвечать всем Вашим запросам</p>
                </div>

                <MainFilters />
            </div>
            <div className='main__slider_item'>
                <p className='main__slider_count title-font'>
                    <span>0{currSlide + 1}</span><span>/ 0{mainPageSliderImages.length}</span>
                </p>
                <div className='main__slider_controls'>
                    <button onClick={goToPrevSlide}>
                        <svg width="20" height="10" viewBox="0 0 20 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" clipRule="evenodd" d="M7.77949 1.06835e-06C6.19195 1.67882 3.8402 3.60178 1.53169 4.41553L-3.30333e-07 4.95546L1.53169 5.49538C3.84196 6.30975 5.92304 7.94937 7.77038 10L8.5497 9.21487C7.24324 7.76465 5.79582 6.4793 4.20599 5.52458L20 5.52458L20 4.38633L4.1699 4.38633C5.85617 3.36772 7.40268 2.00821 8.54059 0.804869L7.77949 1.06835e-06Z" fill="white" />
                        </svg>
                    </button>
                    <button onClick={goToNextSlide}>
                        <svg width="20" height="10" viewBox="0 0 20 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" clipRule="evenodd" d="M12.2205 1.06835e-06C13.808 1.67882 16.1598 3.60178 18.4683 4.41553L20 4.95546L18.4683 5.49538C16.158 6.30975 14.077 7.94937 12.2296 10L11.4503 9.21487C12.7568 7.76465 14.2042 6.4793 15.794 5.52458L-4.82974e-07 5.52458L-3.83465e-07 4.38633L15.8301 4.38633C14.1438 3.36772 12.5973 2.00821 11.4594 0.804869L12.2205 1.06835e-06Z" fill="white" />
                        </svg>
                    </button>
                </div>
            </div>

        </div>
    </div >
}

export default MainPageCarousel