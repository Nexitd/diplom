import { Dispatch, ReactNode, SetStateAction, memo, useEffect, useMemo, useState } from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { Form, Upload } from "antd";
import load from "assets/images/Фото грузится.png"
import { CameraIcon, DeleteFileIcon } from "shared/icons";
import { usePreviewImage } from "../lib";
import 'react-lazy-load-image-component/src/effects/blur.css';

type UploadFileProps = {
    link: string | undefined;
    text?: string;
    icon?: ReactNode;
    name?: string;
    maxLength?: number;
    setFilesCount?: Dispatch<SetStateAction<number>> | any;
}

const UploadFile = ({ link, icon = <CameraIcon />, text = '', name = 'image', maxLength = 1, setFilesCount }: UploadFileProps) => {
    const [isFile, setIsFile] = useState(false)
    const { fileList, handleChange, handlePreview, setFileList, previewImage, setPreviewImage } = usePreviewImage(link)

    useEffect(() => {
        if (link !== '') {
            setIsFile(prev => !prev);
        }
    }, [link])

    const uploadBtn = useMemo(() => {
        return <>
            {icon}
            {text && <p className="upload__container_text">{text}</p>}
        </>
    }, [icon, text])


    return <div className="upload">
        <Form.Item name={name} getValueFromEvent={(fileList) => fileList}>
            {isFile ? <div className="upload__container">
                <button type="button" className="upload__container_btn" onClick={() => {
                    setPreviewImage("");
                    setFileList([]);
                    setFilesCount((prev: number[]) => [...prev.slice(0, prev.length - 1)])
                    setIsFile(prev => !prev)
                }}>
                    <DeleteFileIcon />
                </button>
                <LazyLoadImage src={previewImage} placeholderSrc={load} effect="blur" alt="Image alt" />
            </div> :
                <Upload
                    className="upload__label"
                    fileList={fileList}
                    accept="image/png,image/jpg,image/jpeg"
                    beforeUpload={() => false}
                    onChange={(file) => {
                        handleChange(file);
                        handlePreview(file.file);
                        setIsFile(prev => !prev);
                        setFilesCount((prev: number[]) => [...prev, Math.random() * 10])
                    }}
                >
                    {fileList.length >= maxLength ? null : uploadBtn}
                </Upload>
            }

        </Form.Item>
    </div>
}

export default memo(UploadFile);