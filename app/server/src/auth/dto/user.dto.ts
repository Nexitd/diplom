import {
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsPositive,
  IsString,
} from 'class-validator';

type RoleType = {
  id: number;
  name: string;
};

type UserRolesType = {
  role: RoleType;
};

export class UserDto {
  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  id: number;

  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  user_roles: UserRolesType[];
}
