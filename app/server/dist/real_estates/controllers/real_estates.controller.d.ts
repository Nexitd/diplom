import { RealEstatesService } from '../services/real_estates.service';
import { CreateRealEstateDto } from '../dto/create-real_estate.dto';
import { UpdateRealEstateDto } from '../dto/update-real_estate.dto';
export declare class RealEstatesController {
    private readonly realEstatesService;
    constructor(realEstatesService: RealEstatesService);
    create(createRealEstateDto: CreateRealEstateDto): Promise<import(".prisma/client").real_estate>;
    findAll(): Promise<import(".prisma/client").real_estate[]>;
    findOne(id: string): Promise<{} | import(".prisma/client").real_estate>;
    update(id: string, updateRealEstateDto: UpdateRealEstateDto): string;
    remove(id: string): Promise<any>;
}
