import first from 'assets/images/main/first.png';
import second from 'assets/images/main/second.png';
import third from 'assets/images/main/third.png';
import fourth from 'assets/images/main/fourth.png';
import fifth from 'assets/images/main/fifth.png';

export type SliderImage = {
    image: string;
    id: number;
};

export const mainPageSliderImages: SliderImage[] = [
    {
        id: 0,
        image: first,
    },
    {
        id: 1,
        image: second,
    },
    {
        id: 2,
        image: third,
    },
    {
        id: 3,
        image: fourth,
    },
    {
        id: 4,
        image: fifth,
    },
];
