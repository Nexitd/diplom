"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../prisma/prisma.service");
let UsersService = class UsersService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async findAllUsers() {
        const users = await this.prisma.user.findMany({
            include: {
                user_roles: {
                    select: {
                        role: true,
                    },
                },
            },
        });
        const usersWithoutPassword = users.map((el) => this.excludePassword(el, ['password']));
        return usersWithoutPassword;
    }
    excludePassword(user, keys) {
        for (let key of keys) {
            delete user[key];
        }
        return user;
    }
    async findOneUser(id) {
        const user = await this.prisma.user.findUnique({
            where: { id: id },
            include: {
                user_roles: {
                    select: {
                        role: true,
                    },
                },
                images: {
                    select: {
                        id: true,
                        link: true,
                    },
                },
            },
        });
        const userWithoutPassword = this.excludePassword(user, ['password']);
        return userWithoutPassword;
    }
    async updateUser(id, updateUserDto) {
    }
    async removeUser(id) {
        await this.prisma.user.delete({ where: { id: id } });
    }
};
UsersService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map