import { Injectable, CanActivate, Inject, ExecutionContext } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { IS_PUBLIC } from "../decorators/public.decorator";

@Injectable()
export class LoggedInGuard implements CanActivate {
  constructor(@Inject(Reflector) protected readonly reflector?: Reflector) {}

  canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();

    if (this.reflector) {
      const isPublic = this.reflector.get<string[]>(
        IS_PUBLIC,
        context.getHandler(),
      );

      if (isPublic) {
        return true;
      }
    }

    return request.isAuthenticated();
  }
}
