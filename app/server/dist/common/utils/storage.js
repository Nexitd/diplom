"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.imageFileFilter = exports.storage = void 0;
const common_1 = require("@nestjs/common");
const fs_1 = require("fs");
const path_1 = require("path");
const multer_1 = require("multer");
const uuid_1 = require("uuid");
exports.storage = {
    storage: (0, multer_1.diskStorage)({
        destination: (req, file, callback) => {
            const baseDirectory = "./uploads";
            if (!(0, fs_1.existsSync)(baseDirectory)) {
                (0, fs_1.mkdirSync)(baseDirectory, { recursive: true });
            }
            return callback(null, baseDirectory);
        },
        filename: (req, file, callback) => {
            const fileExtName = (0, path_1.extname)(file.originalname);
            callback(null, `${(0, uuid_1.v4)()}${fileExtName}`);
        },
    }),
};
const imageFileFilter = (req, file, callback) => {
    const extension = (0, path_1.parse)(file.originalname).ext;
    if (!extension.toLocaleLowerCase().match(/\.(jpg|jpeg|bmp|png|gif|pdf)$/)) {
        return callback(new common_1.BadRequestException("Files not allowed!"), false);
    }
    return callback(null, true);
};
exports.imageFileFilter = imageFileFilter;
//# sourceMappingURL=storage.js.map