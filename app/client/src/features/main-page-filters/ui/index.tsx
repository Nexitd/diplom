import { useSearchParams } from "react-router-dom"
import { SearchOutlined } from "@ant-design/icons"
import { Button, DropDown } from "shared/ui"
import { priceTypeItems, roomsCountItems, roomsTypeItems, sectorsItems } from "shared/constants"

const MainFilters = () => {
    const [searchParams, setSearchParams] = useSearchParams();

    const handleChange = (value: string, searchParam: string) => {
        searchParams.set(searchParam, value);
        setSearchParams(searchParams);
    }

    return <div className='filters'>
        <div className='filters__container'>
            <DropDown title={searchParams.get("district") || "Район"} onChange={handleChange} items={sectorsItems} className='filters__container_select' searchParam="district" />
            <DropDown title={searchParams.get("window_view") || "Вид жилья"} onChange={handleChange} items={roomsTypeItems} className='filters__container_select' searchParam="window_view" />
            <DropDown title={searchParams.get("rooms_count") || "Кол-во комнат"} onChange={handleChange} items={roomsCountItems} className='filters__container_select' searchParam="rooms_count" />
            <DropDown title={searchParams.get("price") || "Цена"} onChange={handleChange} items={priceTypeItems} className='filters__container_select' searchParam="price" />
            <Button isImage={true} text="Показать" type="button" icon={<SearchOutlined style={{ fontSize: 23 }} />} className='filters__container_btn' />
        </div>
    </div>
}

export default MainFilters