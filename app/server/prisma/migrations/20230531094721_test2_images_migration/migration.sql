-- AlterTable
ALTER TABLE "images" ALTER COLUMN "real_estate_id" DROP DEFAULT,
ALTER COLUMN "user_id" DROP DEFAULT,
ALTER COLUMN "company_id" DROP DEFAULT;
