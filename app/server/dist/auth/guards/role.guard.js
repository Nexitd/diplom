"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleGuard = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("../services/auth.service");
const auth_guard_1 = require("./auth.guard");
const RoleGuard = (roles) => {
    let RoleGuardMixin = class RoleGuardMixin extends auth_guard_1.LoggedInGuard {
        constructor(authService) {
            super();
            this.authService = authService;
        }
        async canActivate(context) {
            await super.canActivate(context);
            const request = context.switchToHttp().getRequest();
            const user = request.user;
            if (!user) {
                throw new common_1.UnauthorizedException();
            }
            const userRoles = await this.authService.getUserRoles(user.id);
            return userRoles.some((n) => roles.includes(n.role.name));
        }
    };
    RoleGuardMixin = __decorate([
        (0, common_1.Injectable)(),
        __param(0, (0, common_1.Inject)(auth_service_1.AuthService)),
        __metadata("design:paramtypes", [auth_service_1.AuthService])
    ], RoleGuardMixin);
    return (0, common_1.mixin)(RoleGuardMixin);
};
exports.RoleGuard = RoleGuard;
//# sourceMappingURL=role.guard.js.map