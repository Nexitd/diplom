import { useEffect } from "react";
import { Form, Input } from "antd"
import { useNavigate } from "react-router";
import { AuthModel } from "entities/auth";
import { useAppDispatch, useAppSelector } from "shared/api";
import { Button, CustomInput } from "shared/ui";

const MainPageHero = () => {
    const { isError, errorStatus, isSuccess, isAuth } = useAppSelector(state => state.auth);
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const redirectToPersonalArea = () => navigate(`/personal-area`)

    useEffect(() => {
        if (isSuccess) {
            redirectToPersonalArea()
        }
    }, [isSuccess])

    const handleSubmit = (values: { email: string; password: string }) => {
        if (!isAuth) {
            dispatch(AuthModel.Login(values));

            return
        }

        redirectToPersonalArea()
    }

    return <div className="hero">
        <div className="hero__container wrapper__container">
            <div className="hero__container_item">
                <h2 className="title-font hero__title">Если Вы владелец недвижимости</h2>
                <p className="hero__text">авторизуйтесь и разместите свои предложения о недвижимости</p>
            </div>
            <div className="hero__container_item">
                <Form autoComplete="off" onFinish={handleSubmit} className="hero__form">
                    {!isAuth ?
                        <>
                            <CustomInput
                                name="email"
                                placeholder="E-mail"
                                className="hero__form_item"
                                rules={[
                                    { required: true, message: 'Введите email!' },
                                    {
                                        type: 'email',
                                        message: 'Введено не валидное значение!',
                                    }
                                ]}
                            />
                            <Form.Item className="hero__form_item" name="password" rules={[{ required: true, message: 'Введите пароль!' }]}>
                                <Input.Password placeholder="Пароль" />
                            </Form.Item>
                            {isError && <Form.Item className="hero__form_item error" name="error_message">
                                <h2 className="error__message">{errorStatus}</h2>
                            </Form.Item>}
                            <Form.Item>
                                <Button type="submit" text="Войти" />
                            </Form.Item>
                        </>
                        :
                        <Form.Item>
                            <Button type="submit" text="Войти в личный кабинет" />
                        </Form.Item>
                    }
                </Form>
            </div>
        </div>
    </div>
}

export default MainPageHero