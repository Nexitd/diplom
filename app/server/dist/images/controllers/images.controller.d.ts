/// <reference types="multer" />
import { ImagesService } from '../services/images.service';
export declare class ImagesController {
    private readonly imagesService;
    constructor(imagesService: ImagesService);
    create(file: Express.Multer.File, data: {
        user_id: null | number;
        company_id: null | number;
        real_estate_id: null | number;
    }): Promise<import(".prisma/client").images>;
    findAll(): Promise<import(".prisma/client").images[]>;
    remove(id: string): Promise<{
        id: number;
        message: "Successfully removed";
    } | import("@nestjs/common").NotFoundException>;
}
