"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const common_1 = require("@nestjs/common");
const cookieParser = require("cookie-parser");
const app_module_1 = require("./app.module");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule);
    app.enableCors({
        origin: true,
        credentials: true,
        methods: 'GET, PUT, POST, DELETE',
    });
    app.use(cookieParser());
    app.setGlobalPrefix(process.env.GLOBAL_PREFIX, {
        exclude: [{ path: '/uploads/:path', method: common_1.RequestMethod.GET }],
    });
    await app.listen(8080);
}
bootstrap();
//# sourceMappingURL=main.js.map