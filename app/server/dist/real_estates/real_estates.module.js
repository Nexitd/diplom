"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RealEstatesModule = void 0;
const common_1 = require("@nestjs/common");
const real_estates_service_1 = require("./services/real_estates.service");
const real_estates_controller_1 = require("./controllers/real_estates.controller");
const prisma_module_1 = require("../prisma/prisma.module");
let RealEstatesModule = class RealEstatesModule {
};
RealEstatesModule = __decorate([
    (0, common_1.Module)({
        controllers: [real_estates_controller_1.RealEstatesController],
        providers: [real_estates_service_1.RealEstatesService],
        imports: [prisma_module_1.PrismaModule],
    })
], RealEstatesModule);
exports.RealEstatesModule = RealEstatesModule;
//# sourceMappingURL=real_estates.module.js.map