/// <reference types="multer" />
import { Request, Express } from "express";
export declare const storage: {
    storage: import("multer").StorageEngine;
};
export declare const imageFileFilter: (req: Request, file: Express.Multer.File, callback: any) => any;
