import { MainPageAbout } from "widgets/main-page-about-content"
import { MainSlider } from "widgets/main-page-carousel"
import { MainPageHero } from "widgets/main-page-hero"


const Main = () => {
    return (
        <div className='main'>
            <div className='main__hero'>
                <MainSlider />
            </div>

            <div className="main__container wrapper__container">
                <section>
                    <MainPageAbout />
                </section>
            </div>

            <div className="main__footer_hero">
                <MainPageHero />
            </div>
        </div>
    )
}

export default Main