import { CreateRealEstateDto } from './dto/create-real_estate.dto';
import { UpdateRealEstateDto } from './dto/update-real_estate.dto';
export declare class RealEstatesService {
    create(createRealEstateDto: CreateRealEstateDto): string;
    findAll(): string;
    findOne(id: number): string;
    update(id: number, updateRealEstateDto: UpdateRealEstateDto): string;
    remove(id: number): string;
}
