import { BadRequestException } from "@nestjs/common";
import { existsSync, mkdirSync } from "fs";
import { Request, Express } from "express";
import { extname, parse } from "path";
import { diskStorage } from "multer";
import { v4 as uuid } from "uuid";

export const storage = {
    storage: diskStorage({
        destination: (req: Request, file: Express.Multer.File, callback: (error: Error | null, destination: string) => void) => {
            const baseDirectory = "./uploads";

            if (!existsSync(baseDirectory)) {
                mkdirSync(baseDirectory, { recursive: true });
            }
            return callback(null, baseDirectory);
        },
        filename: (req: Request, file: Express.Multer.File, callback: (error: Error | null, filename: string) => void) => {
            const fileExtName = extname(file.originalname);
            callback(null, `${uuid()}${fileExtName}`);
        },
    }),
};

export const imageFileFilter = (req: Request, file: Express.Multer.File, callback) => {
    const extension: string = parse(file.originalname).ext;
    if (!extension.toLocaleLowerCase().match(/\.(jpg|jpeg|bmp|png|gif|pdf)$/)) {
        return callback(new BadRequestException("Files not allowed!"), false);
    }
    return callback(null, true);
};

