import { Carousel } from "antd"
import styles from "./style.module.scss"


type SliderImage = {
    image: string;
    id: number;
}

type SliderProps = {
    className?: string;
    sliderData: SliderImage[]
}


const Slider = ({ className = '', sliderData = [] }: SliderProps) => {
    return (
        <Carousel className={className}>
            {sliderData.map(el => {
                return <div key={el.id} className={styles.slider__item}>
                    <img src={el.image} alt="" />
                </div>

            })}
        </Carousel>
    )
}

export default Slider