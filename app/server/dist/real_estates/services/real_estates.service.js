"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RealEstatesService = void 0;
const common_1 = require("@nestjs/common");
const prisma_service_1 = require("../../prisma/prisma.service");
let RealEstatesService = class RealEstatesService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    async create(realEstate) {
        const newObject = await this.prisma.real_estate.create({
            data: Object.assign({}, realEstate),
        });
        return newObject;
    }
    async findAllObjects() {
        const objects = this.prisma.real_estate.findMany({});
        return objects;
    }
    async findOne(id) {
        const object = await this.prisma.real_estate.findUnique({
            where: { id: id },
        });
        if (!object)
            throw new common_1.BadRequestException();
        return object;
    }
    update(id, updateRealEstateDto) {
        return `This action updates a #${id} realEstate`;
    }
    async remove(id) {
        await this.prisma.real_estate.delete({ where: { id: id } });
        return { success: true };
    }
};
RealEstatesService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService])
], RealEstatesService);
exports.RealEstatesService = RealEstatesService;
//# sourceMappingURL=real_estates.service.js.map