import { useState, useEffect } from "react";
import type { RcFile, UploadProps } from 'antd/es/upload';
import type { UploadFile } from 'antd/es/upload/interface';
import { uploadFilesUrl } from "shared/config";

const getBase64 = (file: RcFile): Promise<string> =>
    new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result as string);
        reader.onerror = (error) => reject(error);
    });

export const usePreviewImage = (link: string | undefined) => {
    // const [imagePreview, setImagePreview] = useState<string>("");
    const [fileList, setFileList] = useState<UploadFile[]>([]);
    const [previewImage, setPreviewImage] = useState<string>('')

    useEffect(() => {
        if (link) {
            setPreviewImage(uploadFilesUrl + link)
        }
    }, [link])

    // function previewFile(e: any) {
    //     const reader = new FileReader();

    //     const selectedFile = e.target.files[0];
    //     if (selectedFile) {
    //         reader.readAsDataURL(selectedFile);
    //     }

    //     reader.onload = (readerEvent: any) => {
    //         setImagePreview(readerEvent.target.result);
    //     };
    // }


    // return { previewFile, imagePreview, setImagePreview }

    const handlePreview = async (file: UploadFile) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file as RcFile)
            setPreviewImage(file.url || (file.preview as string));
        }
    };

    const handleChange: UploadProps['onChange'] = ({ fileList: newFileList }) =>
        setFileList(newFileList);

    return { fileList, handleChange, setFileList, handlePreview, previewImage, setPreviewImage }
}