import { RealEstatesService } from './real_estates.service';
import { CreateRealEstateDto } from './dto/create-real_estate.dto';
import { UpdateRealEstateDto } from './dto/update-real_estate.dto';
export declare class RealEstatesController {
    private readonly realEstatesService;
    constructor(realEstatesService: RealEstatesService);
    create(createRealEstateDto: CreateRealEstateDto): any;
    findAll(): any;
    findOne(id: string): any;
    update(id: string, updateRealEstateDto: UpdateRealEstateDto): any;
    remove(id: string): any;
}
