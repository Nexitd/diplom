export declare class CreateUserDto {
    name: string;
    surname: string;
    patronymic: string;
    email: string;
    phone: string;
    password: string;
}
