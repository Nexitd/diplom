import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { user as User } from '@prisma/client';
import { compare, hash } from 'bcrypt';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { LoginUserDto } from 'src/users/dto/login-user.dto';
import { UserDto } from '../dto/user.dto';

@Injectable()
export class AuthService {
  constructor(private prisma: PrismaService, private jwtService: JwtService) {}

  async generateToken(user: UserDto) {
    const roleName = user.user_roles.map((el) => {
      return el.role.name;
    })[0];
    
    const payload = { id: user.id, role: roleName };

    return {
      status: 200,
      token: this.jwtService.sign(payload),
    };
  }

  async validateUser(user: LoginUserDto) {
    const { email, password } = user;
    const foundUser = await this.findByEmail(email.toLowerCase());

    if (
      !foundUser ||
      !(await compare(password, foundUser.password).then((result) => result))
    ) {
      throw new UnauthorizedException('Неверный логин или пароль');
    }

    const { password: _password, ...retUser } = foundUser;

    return retUser;
  }

  async login(userData: LoginUserDto) {
    const user = await this.validateUser(userData);
    const token = await this.generateToken(user);

    return token;
  }

  async registerUser(user: CreateUserDto) {
    const { email, name, surname, patronymic, password, phone } = user;
    const existingUser = await this.findByEmail(email);

    if (existingUser) {
      throw new BadRequestException('E-mail должен быть уникален');
    }

    const role = await this.prisma.role.findFirst({
      where: {
        name: 'USER',
      },
    });

    const newUser = await this.prisma.user.create({
      data: {
        email: email.toLowerCase(),
        name: name,
        surname: surname,
        patronymic: patronymic,
        phone: phone,
        user_roles: {
          create: {
            roleId: role.id,
          },
        },
        password: await hash(password, 12),
      },
    });

    delete newUser.password;

    return newUser;
  }

  async findByEmail(email: string): Promise<User & { user_roles: any }> {
    const user = await this.prisma.user.findFirst({
      where: {
        email: email,
      },
      include: {
        user_roles: {
          select: {
            role: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });

    if (!user) {
      return null;
    }

    return user;
  }

  async findById(id: number): Promise<Omit<User, 'password'>> {
    const { ...user } = await this.prisma.user.findUnique({
      where: {
        id: id,
      },
      include: {
        user_roles: {
          select: {
            role: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });

    if (!user) {
      throw new BadRequestException(`No user found with id ${id}`);
    }
    return user;
  }

  async getUserRoles(userId: number) {
    const user = await this.prisma.user_roles.findMany({
      where: {
        userId: userId,
      },
      include: {
        role: {
          select: {
            name: true,
          },
        },
      },
    });

    return user;
  }
}
