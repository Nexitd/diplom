import { Strategy } from 'passport-local';
import { AuthService } from '../services/auth.service';
declare const LocalStrategy_base: new (...args: any[]) => Strategy;
export declare class LocalStrategy extends LocalStrategy_base {
    private readonly authService;
    constructor(authService: AuthService);
    validate(email: string, password: string): Promise<{
        id: number;
        email: string;
        name: string;
        surname: string;
        phone: string;
        patronymic: string;
        birth_date: Date;
        CreatedAt: Date;
        UpdatedAt: Date;
        user_roles: any;
    }>;
}
export {};
