import { PartialType } from '@nestjs/mapped-types';
import { CreateRealEstateDto } from './create-real_estate.dto';
import { IsString, IsNumber, IsNotEmpty, IsPositive } from 'class-validator';

export class UpdateRealEstateDto extends PartialType(CreateRealEstateDto) {
  @IsString()
  title: string;

  @IsString()
  address: string;

  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  price: number;

  @IsString()
  description: string;

  @IsString()
  housing_type: string;

  @IsNumber()
  @IsPositive()
  rooms_count: number;

  @IsNumber()
  @IsPositive()
  total_area: number;
  @IsNumber()
  @IsPositive()
  living_space: number;
  @IsNumber()
  @IsPositive()
  kitchen_area: number;
  @IsNumber()
  @IsPositive()
  celling_height: number;
  @IsNumber()
  @IsPositive()
  floor: number;
  @IsString()
  window_view: string;
  @IsString()
  repair: string;
  @IsNumber()
  @IsPositive()
  balcony: number;

  @IsString()
  house_type: string;
  @IsNumber()
  @IsPositive()
  lifts_count: number;
  @IsNumber()
  @IsPositive()
  floors_count: number;
  @IsNumber()
  @IsPositive()
  construction_year: number;
  door_phone: boolean;
  concierge: boolean;
  close_area: boolean;
  code_door: boolean;

  @IsString()
  parking: string;
  @IsString()
  landscaping: string;
  @IsString()
  infrastruktura: string;

  gas: boolean;
  garbage_chute: boolean;

  @IsNumber()
  @IsPositive()
  years_in_ownership: number;
  @IsNumber()
  @IsPositive()
  owners_count: number;
  @IsNumber()
  @IsPositive()
  spelled_out_man_count: number;

  have_a_buyer: boolean;
  bargaining_is_appropriate: boolean;
}
