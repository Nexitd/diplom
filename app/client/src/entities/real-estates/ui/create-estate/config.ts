// config data for inputs type text

export const formItemsBase = [
    {
        id: 112,
        name: 'title',
        placeholder: 'Введите название объекта',
        rules: [
            {
                required: true,
                message: 'Заполните поле!',
            },
        ],
    },
    {
        id: 2231,
        name: 'address',
        placeholder: 'Введите адрес объекта',
        rules: [
            {
                required: true,
                message: 'Заполните поле!',
            },
        ],
    },
];

export const formItemsFlat = [
    {
        id: 21331,
        name: 'total_area',
        placeholder: 'Общая площадь',
        rules: [
            {
                required: true,
                message: 'Заполните поле!',
            },
            {
                pattern: /^(?:\d*)$/,
                message: 'Значение должно быть числом!',
            },
        ],
    },
    {
        id: 4332,
        name: 'living_space',
        placeholder: 'Жилая площадь',
        rules: [
            {
                required: true,
                message: 'Заполните поле!',
            },
            {
                pattern: /^(?:\d*)$/,
                message: 'Значение должно быть числом!',
            },
        ],
    },
    {
        id: 4144,
        name: 'kitchen_area',
        placeholder: 'Площадь кухни',
        rules: [
            {
                required: true,
                message: 'Заполните поле!',
            },
            {
                pattern: /^(?:\d*)$/,
                message: 'Значение должно быть числом!',
            },
        ],
    },
    {
        id: 51421,
        name: 'celling_height',
        placeholder: 'Высота потолков',
        rules: [
            {
                required: true,
                message: 'Заполните поле!',
            },
            {
                pattern: /^(?:\d*)$/,
                message: 'Значение должно быть числом!',
            },
        ],
    },
    {
        id: 64124,
        name: 'floor',
        placeholder: 'Этаж',
        rules: [
            {
                required: true,
                message: 'Заполните поле!',
            },
            {
                pattern: /^(?:\d*)$/,
                message: 'Значение должно быть числом!',
            },
        ],
    },
    {
        id: 732432,
        name: 'window_view',
        placeholder: 'Вид из окон',
        rules: [
            {
                required: true,
                message: 'Заполните поле!',
            },
        ],
    },
];

export const formItemsHome = [
    {
        id: 889,
        name: 'lifts_count',
        placeholder: 'Количество лифтов в подъезде',
        rules: [
            {
                pattern: /^(?:\d*)$/,
                message: 'Значение должно быть числом!',
            },
        ],
    },
    {
        id: 9909,
        name: 'floors_count',
        placeholder: 'Количество этажей',
        rules: [
            { required: true, message: 'Заполните поле!' },
            {
                pattern: /^(?:\d*)$/,
                message: 'Значение должно быть числом!',
            },
        ],
    },
    {
        id: 1032,
        name: 'construction_year',
        placeholder: 'Год постройки',
        rules: [
            {
                pattern: /^(?:\d*)$/,
                message: 'Значение должно быть числом!',
            },
        ],
    },
];

// config data for select inputs

export const housingTypeSelectData = [
    {
        id: 1324124,
        label: 'Новостройка',
        value: 'Новостройка',
    },
    {
        id: 123124324,
        label: 'Вторичка',
        value: 'Вторичка',
    },
];

export const houseTypeSelectData = [
    {
        id: 432423,
        label: 'Монолитный',
        value: 'Монолитный',
    },
    {
        id: 543532,
        label: 'Кирпично-монолитный',
        value: 'Кирпично-монолитный',
    },
    {
        id: 52313,
        label: 'Кирпичный',
        value: 'Кирпичный',
    },
    {
        label: 'Панельный',
        value: 'Панельный',
    },
];

export const roomsCountSelectData = [
    {
        id: 54123,
        label: 'Студия',
        value: 'Студия',
    },
    {
        id: 556432,
        label: '1',
        value: '1',
    },
    {
        id: 8989121,
        label: '2',
        value: '2',
    },
    {
        id: 5632131,
        label: '3',
        value: '3',
    },
    {
        id: 677553,
        label: '4+',
        value: '4+',
    },
];

export const repairSelectData = [
    {
        id: 134432,
        label: 'Без ремонта',
        value: 'Без ремонта',
    },
    {
        id: 554221,
        label: 'Евроремонт',
        value: 'Евроремонт',
    },
    {
        id: 9990099,
        label: 'Дизайнерский ремонт',
        value: 'Дизайнерский ремонт',
    },
    {
        id: 114442,
        label: 'Косметический ремонт',
        value: 'Косметический ремонт',
    },
];

export const balconySelectData = [
    {
        id: 44441112,
        label: '1',
        value: '1',
    },
    {
        id: 55533311,
        label: '2',
        value: '2',
    },
    {
        id: 5553222,
        label: '3+',
        value: '3+',
    },
];

export const ownersCountSelectData = [
    {
        id: 555331,
        label: '1',
        value: '1',
    },
    {
        id: 666333,
        label: '2',
        value: '2',
    },
    {
        id: 54421,
        label: '3+',
        value: '3+',
    },
];

// config data for checkbox inputs

export const extraChecksData = [
    {
        id: 1,
        title: 'Домофон',
        name: 'doorphone',
        checks: [
            {
                id: 11,
                label: 'есть',
                value: 'есть',
            },
        ],
    },
    {
        id: 2,
        title: 'Консьерж',
        name: 'concierge',
        checks: [
            {
                id: 22,
                label: 'есть',
                value: 'есть',
            },
        ],
    },
    {
        id: 3,
        title: 'Закрытая территория',
        name: 'close_area',
        checks: [
            {
                id: 33,
                label: 'есть',
                value: 'есть',
            },
        ],
    },
    {
        id: 4,
        title: 'Кодовая дверь',
        name: 'code_door',
        checks: [
            {
                id: 44,
                label: 'есть',
                value: 'есть',
            },
        ],
    },
    {
        id: 5,
        title: 'Парковка',
        name: 'parking',
        checks: [
            {
                id: 55,
                label: 'во дворе',
                value: 'во дворе',
            },
            {
                id: 66,
                label: 'подземная',
                value: 'подземная',
            },
            {
                id: 77,
                label: 'охраняемая',
                value: 'охраняемая',
            },
            {
                id: 88,
                label: 'со шлагбаумом',
                value: 'со шлагбаумом',
            },
        ],
    },
    {
        id: 6,
        title: 'Благоустройство двора',
        name: 'landscaping',
        checks: [
            {
                id: 99,
                label: 'детская площадка',
                value: 'детская площадка',
            },
            {
                id: 101,
                label: 'спортивная площадка',
                value: 'спортивная площадка',
            },
        ],
    },
    {
        id: 7,
        title: 'Инфраструктура',
        name: 'infrastruktura',
        checks: [
            {
                id: 112,
                label: 'школа',
                value: 'школа',
            },
            {
                id: 122,
                label: 'детский сад',
                value: 'детский сад',
            },
            {
                id: 133,
                label: 'фитнесс',
                value: 'фитнесс',
            },
            {
                id: 144,
                label: 'парк',
                value: 'парк',
            },
            {
                id: 155,
                label: 'торговый центр',
                value: 'торговый центр',
            },
            {
                id: 166,
                label: 'больница',
                value: 'больница',
            },
        ],
    },
];
