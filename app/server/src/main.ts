import { NestFactory } from '@nestjs/core';
import { RequestMethod } from '@nestjs/common';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({
    origin: true,
    credentials: true,
    methods: 'GET, PUT, POST, DELETE',
  });

  app.use(cookieParser());
  app.setGlobalPrefix(process.env.GLOBAL_PREFIX, {
    exclude: [{ path: '/uploads/:path', method: RequestMethod.GET }],
  });

  await app.listen(8080);
}

bootstrap();
