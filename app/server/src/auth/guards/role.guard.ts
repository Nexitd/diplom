import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
  Type,
  UnauthorizedException,
  mixin,
} from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { LoggedInGuard } from './auth.guard';

type userRole = 'USER' | 'DIRECTOR' | 'LAWYER' | 'REALTOR' | 'MODER';

const RoleGuard = (roles: userRole[]): Type<CanActivate> => {
  @Injectable()
  class RoleGuardMixin extends LoggedInGuard {
    constructor(
      @Inject(AuthService) protected readonly authService: AuthService,
    ) {
      super();
    }

    async canActivate(context: ExecutionContext) {
      await super.canActivate(context);

      const request = context.switchToHttp().getRequest();
      const user = request.user;

      if (!user) {
        throw new UnauthorizedException();
      }

      const userRoles = await this.authService.getUserRoles(user.id);
      
      return userRoles.some((n: any) => roles.includes(n.role.name));
    }
  }

  return mixin(RoleGuardMixin);
};

export { RoleGuard };
