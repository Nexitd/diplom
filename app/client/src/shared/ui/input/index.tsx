import { memo } from "react"
import { Input } from "antd"
import FormItem from "antd/es/form/FormItem"
import cn from 'classnames';

type CustomInputPopsType = {
    placeholder?: string;
    className?: string;
    name: string;
    type?: string;
    [x: string]: any;
}

const CustomInput = ({ placeholder = '', className = '', type = 'text', name, ...rest }: CustomInputPopsType) => {
    return <FormItem name={name} className={cn(className, 'form__input')} {...rest}>
        <Input placeholder={placeholder} type={type} />
    </FormItem >
}

export default memo(CustomInput)