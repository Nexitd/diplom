import { CanActivate, Type } from '@nestjs/common';
declare type userRole = 'USER' | 'DIRECTOR' | 'LAWYER' | 'REALTOR' | 'MODER';
declare const RoleGuard: (roles: userRole[]) => Type<CanActivate>;
export { RoleGuard };
