import { PrismaService } from '../../prisma/prisma.service';
import { UpdateUserDto } from '../dto/update-user.dto';
import { user } from '@prisma/client';
export declare class UsersService {
    private readonly prisma;
    constructor(prisma: PrismaService);
    findAllUsers(): Promise<Omit<user, 'password'>[]>;
    excludePassword<User, Key extends keyof User>(user: User, keys: Key[]): Omit<User, Key>;
    findOneUser(id: number): Promise<Omit<user, 'password'> | null>;
    updateUser(id: number, updateUserDto: UpdateUserDto): Promise<void>;
    removeUser(id: number): Promise<void>;
}
