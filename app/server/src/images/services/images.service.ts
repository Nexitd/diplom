import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import * as path from 'path';
import * as fs from 'fs';
import { images as ImageModel } from '@prisma/client';

@Injectable()
export class ImagesService {
  constructor(private readonly prisma: PrismaService) {}

  async addFile(
    file: Express.Multer.File,
    sendedData:
      | { user_id: number }
      | { company_id: number }
      | { real_estate_id: number },
  ): Promise<ImageModel> {
    const queryData = {
      ...Object.keys(sendedData).map((key) => {
        if (sendedData[key]) {
          return { [key]: Number(sendedData[key]) };
        } else return { [key]: sendedData[key] };
      })[0],
    };

    const createdFile: ImageModel = await this.prisma.images.create({
      data: {
        ...queryData,
        name: file.filename,
        link: `/${file.path.replace(/\\/g, '/')}`,
      },
    });

    return createdFile;
  }

  async findAll(): Promise<ImageModel[]> {
    const files: ImageModel[] = await this.prisma.images.findMany();
    return files;
  }

  async remove(
    id: number,
  ): Promise<
    { id: number; message: 'Successfully removed' } | NotFoundException
  > {
    const image: ImageModel = await this.prisma.images.findUnique({
      where: {
        id: id,
      },
    });

    if (image) {
      await this.prisma.images.delete({
        where: {
          id: id,
        },
      });

      fs.unlinkSync(path.resolve('./' + image.link));

      return { id: image.id, message: 'Successfully removed' };
    }

    throw new NotFoundException('image.not_found');
  }
}
