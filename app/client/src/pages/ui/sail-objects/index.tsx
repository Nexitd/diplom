import { CreateEstate } from "entities/real-estates";
import { PrivateWrapper } from "../../lib/private-wrapper";

const SailObjects = () => {
    return <div className="object wrapper__container">
        <PrivateWrapper>
            <h1 className="personal__area-title">Новый объект</h1>

            <CreateEstate />
        </PrivateWrapper>
    </div>
}

export default SailObjects;