"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const bcrypt_1 = require("bcrypt");
const prisma_service_1 = require("../../prisma/prisma.service");
let AuthService = class AuthService {
    constructor(prisma, jwtService) {
        this.prisma = prisma;
        this.jwtService = jwtService;
    }
    async generateToken(user) {
        const roleName = user.user_roles.map((el) => {
            return el.role.name;
        })[0];
        const payload = { id: user.id, role: roleName };
        return {
            status: 200,
            token: this.jwtService.sign(payload),
        };
    }
    async validateUser(user) {
        const { email, password } = user;
        const foundUser = await this.findByEmail(email.toLowerCase());
        if (!foundUser ||
            !(await (0, bcrypt_1.compare)(password, foundUser.password).then((result) => result))) {
            throw new common_1.UnauthorizedException('Неверный логин или пароль');
        }
        const { password: _password } = foundUser, retUser = __rest(foundUser, ["password"]);
        return retUser;
    }
    async login(userData) {
        const user = await this.validateUser(userData);
        const token = await this.generateToken(user);
        return token;
    }
    async registerUser(user) {
        const { email, name, surname, patronymic, password, phone } = user;
        const existingUser = await this.findByEmail(email);
        if (existingUser) {
            throw new common_1.BadRequestException('E-mail должен быть уникален');
        }
        const role = await this.prisma.role.findFirst({
            where: {
                name: 'USER',
            },
        });
        const newUser = await this.prisma.user.create({
            data: {
                email: email.toLowerCase(),
                name: name,
                surname: surname,
                patronymic: patronymic,
                phone: phone,
                user_roles: {
                    create: {
                        roleId: role.id,
                    },
                },
                password: await (0, bcrypt_1.hash)(password, 12),
            },
        });
        delete newUser.password;
        return newUser;
    }
    async findByEmail(email) {
        const user = await this.prisma.user.findFirst({
            where: {
                email: email,
            },
            include: {
                user_roles: {
                    select: {
                        role: {
                            select: {
                                name: true,
                            },
                        },
                    },
                },
            },
        });
        if (!user) {
            return null;
        }
        return user;
    }
    async findById(id) {
        const user = __rest(await this.prisma.user.findUnique({
            where: {
                id: id,
            },
            include: {
                user_roles: {
                    select: {
                        role: {
                            select: {
                                name: true,
                            },
                        },
                    },
                },
            },
        }), []);
        if (!user) {
            throw new common_1.BadRequestException(`No user found with id ${id}`);
        }
        return user;
    }
    async getUserRoles(userId) {
        const user = await this.prisma.user_roles.findMany({
            where: {
                userId: userId,
            },
            include: {
                role: {
                    select: {
                        name: true,
                    },
                },
            },
        });
        return user;
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [prisma_service_1.PrismaService, jwt_1.JwtService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map