// auth types

export type LoginDataType = {
    email: string;
    password: string;
};

export type LoginResponseType = {
    status: number;
    token: string;
};

// main page select types

export type selectItemsType = {
    label: string;
    value: string;
};

// user types

export enum roles {
    User = 'USER',
    Director = 'DIRECTOR',
    Lawyer = 'LAWYER',
    Realtor = 'REALTOR',
    Moder = 'MODER',
}

type UserImagesType = {
    id: number;
    link: string;
};

export type UserType = {
    id: number;

    email: string;
    phone: string;

    name: string;
    surname: string;
    patronymic: string;

    birth_date: Date | string;
    CreatedAt: Date | string;

    role: roles;

    images: UserImagesType;
};
