import { lazy } from "react";
import { roles } from "shared/types";
import { PrivateRouteType, RouteType } from "../model";

export const routes: (PrivateRouteType & RouteType & { id: number })[] = [
    {
        id: 1,
        path: '/',
        isPublic: true,
        element: lazy(() => import('pages/ui/main')),
        roles: [roles.User, roles.Realtor, roles.Director, roles.Moder, roles.Lawyer],
    },
    {
        id: 2,
        path: '/catalog',
        isPublic: true,
        element: lazy(() => import('pages/ui/catalog')),
        roles: [roles.User, roles.Realtor, roles.Director, roles.Moder, roles.Lawyer],
    },
    {
        id: 3,
        path: '/personal-area',
        isPublic: false,
        element: lazy(() => import('pages/ui/personal-area')),
        roles: [roles.User, roles.Realtor, roles.Director, roles.Moder, roles.Lawyer],
    },
    {
        id: 4,
        path: '/employees',
        isPublic: false,
        element: lazy(() => import('pages/ui/employees')),
        roles: [roles.Director, roles.Moder],
    },
    {
        id: 5,
        path: '/sail-objects',
        isPublic: false,
        element: lazy(() => import('pages/ui/sail-objects')),
        roles: [roles.User],
    },


]