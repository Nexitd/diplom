import { FC } from "react";
import { Navigate } from "react-router";
import { parseJwt, useAppSelector } from "shared/api";
import { PrivateRouteType, RouteType } from "../model";

export const PrivateRoute: FC<Omit<PrivateRouteType, "path"> & RouteType> = ({ element: RouteComponent, roles, isPublic }) => {
    const userToken = localStorage.getItem("access_token");
    const { isAuth } = useAppSelector(state => state.auth);
    const { role } = parseJwt(userToken);


    const renderRoute = () => {
        if (isPublic) {
            return <RouteComponent />
        } else if (isAuth && role && roles.includes(role)) {
            return <RouteComponent />
        }

        return <Navigate to="/" />
    }

    return renderRoute();
}
