import { useState } from "react"
import { Checkbox, Form, Input, Row, Col } from "antd"
import { UploadFile } from "features/upload-file"
import { RealEstateModel } from "entities/real-estates"
import { Button, CustomInput, DropDown } from "shared/ui"
import { PlusIcon } from "shared/icons"
import {
    balconySelectData,
    extraChecksData,
    formItemsBase,
    formItemsFlat,
    formItemsHome,
    houseTypeSelectData,
    housingTypeSelectData,
    ownersCountSelectData,
    repairSelectData,
    roomsCountSelectData
} from "./config"

export const CreateEstate = () => {
    const [addRealEstate] = RealEstateModel.useAddRealEstateMutation();
    const onFinish = (values: any) => {
        // addRealEstate(values);
        console.log(values)
    }

    const [filesCount, setFilesCount] = useState<number[]>([1])

    return <div className="real__estate">
        <Form className="form" onFinish={onFinish}>
            <h2 className="real__estate_subtitle">Общая информация</h2>
            <div className="real__estate_images">
                <UploadFile
                    link=""
                    name="main_image"
                    text="Добавьте главную фотографию объекта и загрузите дополнительные фото"
                />

                <div className="images__container">
                    {filesCount.map((el, index) => {
                        if (index < 6) {
                            return <UploadFile key={el} setFilesCount={setFilesCount} name={`extra_images-${index}`} link="" icon={<PlusIcon />} maxLength={1} />
                        }
                    })}
                </div>
            </div>

            {formItemsBase.map(el => (
                <CustomInput
                    style={{ width: "50%" }}
                    key={el.id}
                    className="form_item"
                    name={el.name}
                    placeholder={el.placeholder}
                    rules={el.rules}
                />
            ))}

            <Form.Item className="form_item" name="description" rules={[{ required: true, message: "Заполните поле!" }]}>
                <Input.TextArea rows={10} placeholder="Расскажите о недвижимости, собственниках, соседях, транспортной доступности и инфраструктуре" />
            </Form.Item>

            <h2 className="real__estate_subtitle">Информация об объекте</h2>

            <div className="real__estate_container">
                <div className="real__estate_item">
                    <h3 className="real__estate_label">О квартире</h3>
                    <Form.Item className="form_item" name="housing_type">
                        <DropDown className="real__estate_dropdown" searchParam="" title="Тип жилья" items={housingTypeSelectData} />
                    </Form.Item>
                    <Form.Item className="form_item" name="rooms_count">
                        <DropDown className="real__estate_dropdown" searchParam="" title="Количество комнат" items={roomsCountSelectData} />
                    </Form.Item>

                    {formItemsFlat.map(el => (
                        <CustomInput className="form_item" key={el.id} name={el.name} placeholder={el.placeholder} rules={el.rules} />
                    ))}

                    <Form.Item className="form_item" name="repair">
                        <DropDown className="real__estate_dropdown" searchParam="" title="Ремонт" items={repairSelectData} />
                    </Form.Item>
                    <Form.Item className="form_item" name="balcony">
                        <DropDown className="real__estate_dropdown" searchParam="" title="Балкон/лоджия" items={balconySelectData} />
                    </Form.Item>

                    <h3 className="real__estate_label">Коммуникации и удобства</h3>

                    <Row gutter={[24, 24]}>
                        <Col span={12}>
                            <h4 className="check__title">Газ</h4>
                            <Form.Item className="form_item" name="gas" valuePropName="checked" >
                                <Checkbox>
                                    есть
                                </Checkbox>
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <h4 className="check__title">Мусоропровод</h4>
                            <Form.Item className="form_item" name="garbage_chute" valuePropName="checked" >
                                <Checkbox>
                                    есть
                                </Checkbox>
                            </Form.Item>
                        </Col>
                    </Row>

                    <h2 className="real__estate_subtitle">Сделка, цена и условия продажи</h2>

                    <CustomInput className="form_item" name="years_in_ownership" placeholder="Сколько лет в собственности" rules={[{
                        required: true,
                        message: 'Заполните поле!',
                    },
                    {
                        pattern: /^(?:\d*)$/,
                        message: 'Значение должно быть числом!',
                    },]} />


                    <Form.Item className="form_item" name="owners_count">
                        <DropDown searchParam="" className="real__estate_dropdown" items={ownersCountSelectData} title="Собственники" />
                        {/* <Checkbox className="form_item-checkbox">есть несовершеннолетние</Checkbox> */}
                    </Form.Item>


                    <Form.Item className="form_item" name="select">
                        <DropDown className="real__estate_dropdown" searchParam="" items={ownersCountSelectData} title="Прописано человек" />
                        {/* <Checkbox className="form_item-checkbox">есть несовершеннолетние</Checkbox> */}
                    </Form.Item>


                    <CustomInput className="form_item" name="price" placeholder="Цена объекта" rules={[{
                        required: true,
                        message: 'Заполните поле!',
                    },
                    {
                        pattern: /^(?:\d*)$/,
                        message: 'Значение должно быть числом!',
                    },]} />

                    <h4 className="check__title">Условия</h4>
                    <Form.Item className="form_item" name="have_a_buyer" valuePropName="checked">
                        <Checkbox>
                            у меня уже есть покупатель
                        </Checkbox>
                    </Form.Item>
                    <Form.Item className="form_item" name="bargaining_is_appropriate" valuePropName="checked">
                        <Checkbox>
                            торг уместен
                        </Checkbox>
                    </Form.Item>


                    <Button className="real__estate_btn" type="submit" text="Выставить объект на продажу" />
                </div>
                <div className="real__estate_item">
                    <h3 className="real__estate_label">О доме</h3>
                    <Form.Item className="form_item" name="house_type">
                        <DropDown className="real__estate_dropdown" searchParam="" items={houseTypeSelectData} title="Тип дома" />
                    </Form.Item>

                    {formItemsHome.map(el => (
                        <CustomInput className="form_item" key={el.id} name={el.name} rules={el.rules} placeholder={el.placeholder} />
                    ))}

                    <h3 className="real__estate_label">Дополнительно</h3>

                    <Row>
                        {extraChecksData.map(el => (
                            <Col key={el.id} span={el.checks.length === 1 ? 12 : 24}>
                                <h4 className="check__title">{el.title}</h4>
                                <Form.Item style={{ marginRight: 12 }} className="form_item" name={el.name}>
                                    <Checkbox.Group onChange={(checkedValues) => checkedValues}>
                                        <Row>
                                            {el.checks.map(elem => (
                                                <Col key={elem.id}>
                                                    <Checkbox value={elem.value}>{elem.label}</Checkbox>
                                                </Col>
                                            ))}
                                        </Row>
                                    </Checkbox.Group>
                                </Form.Item>
                            </Col>
                        ))}
                    </Row>
                </div>
            </div >
        </Form >
    </div >
}