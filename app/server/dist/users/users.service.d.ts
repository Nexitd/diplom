import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Users } from './users.model';
export declare class UsersService {
    private usersRepository;
    constructor(usersRepository: Repository<Users>);
    createUser(dto: CreateUserDto): Promise<Users>;
    findAllUsers(): Promise<Users[]>;
    findOneUser(id: number): Promise<Users>;
    updateUser(id: number, updateUserDto: UpdateUserDto): Promise<void>;
    removeUser(id: number): Promise<void>;
}
