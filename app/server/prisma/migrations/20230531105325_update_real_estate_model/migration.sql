-- DropIndex
DROP INDEX "images_real_estate_id_key";

-- AlterTable
ALTER TABLE "real_estate" ADD COLUMN     "owners_count" INTEGER DEFAULT 1;
