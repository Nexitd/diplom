import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { baseUrl } from 'shared/config';
import { UserType } from 'shared/types';

export const userApi = createApi({
    reducerPath: 'user',
    baseQuery: fetchBaseQuery({ baseUrl: baseUrl, credentials: 'include' }),
    tagTypes: ['User'],
    endpoints: (build) => ({
        getMe: build.query<UserType, string>({
            query: (id) => `users/${id}`,
            providesTags: (result) => ['User'],
        }),

        deleteUserImage: build.mutation<void, number>({
            query: (id) => ({ url: `images/${id}`, method: 'DELETE' }),
            invalidatesTags: ['User'],
        }),

        addUserImages: build.mutation<any, any>({
            query: (data) => ({
                url: `images?user_id=${data.user_id}`,
                method: 'POST',
                body: data.file,
            }),
            invalidatesTags: ['User'],
        }),

        updateUsersData: build.mutation<UserType, any>({
            query: (data) => ({
                url: `users/${data.id}`,
                method: 'PUT',
                body: data,
            }),
            invalidatesTags: ['User'],
        }),
    }),
});

export const {
    useGetMeQuery,
    useLazyGetMeQuery,
    useAddUserImagesMutation,
    useDeleteUserImageMutation,
    useUpdateUsersDataMutation,
} = userApi;
