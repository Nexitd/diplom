import { FC } from "react";
import { Route } from "react-router";
import { PrivateRouteType, RouteType } from "../model";
import { PrivateRoute } from "./private-route";

export const CreateRoute: FC<RouteType & PrivateRouteType & { id: number }> = ({ element, path, roles, isPublic, id, ...route }) => {
    return (
        <Route
            key={id}
            path={path}
            element={<PrivateRoute isPublic={isPublic} element={element} roles={roles} />}
            {...route}
        />
    );
};