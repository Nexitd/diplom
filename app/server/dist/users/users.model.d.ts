import { Model } from 'sequelize';
interface UserCreationAttrs {
    name: string;
    surname: string;
    patronymic: string;
    email: string;
    password: string;
    role: number;
}
export declare class Users extends Model<Users, UserCreationAttrs> {
    id: number;
    name: string;
    surname: string;
    patronymic: string;
    password: string;
    email: string;
    roleId: number;
}
export {};
