-- AlterTable
ALTER TABLE "user" ADD COLUMN     "birth_date" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
ALTER COLUMN "password" DROP NOT NULL,
ALTER COLUMN "CreatedAt" SET DATA TYPE TIMESTAMP(3),
ALTER COLUMN "UpdatedAt" DROP DEFAULT,
ALTER COLUMN "UpdatedAt" SET DATA TYPE TIMESTAMP(3);

-- CreateTable
CREATE TABLE "real_estate" (
    "id" SERIAL NOT NULL,
    "price" INTEGER NOT NULL,
    "description" TEXT NOT NULL,
    "address" VARCHAR(60) NOT NULL,
    "title" VARCHAR(60) NOT NULL,
    "housing_type" VARCHAR(30) NOT NULL,
    "rooms_count" INTEGER NOT NULL,
    "total_area" INTEGER NOT NULL,
    "living_space" INTEGER NOT NULL,
    "kitchen_area" INTEGER NOT NULL,
    "celling_height" DOUBLE PRECISION NOT NULL,
    "floor" INTEGER NOT NULL,
    "window_view" TEXT NOT NULL,
    "repair" VARCHAR(30) NOT NULL,
    "balcony" INTEGER NOT NULL,
    "house_type" VARCHAR(60) NOT NULL,
    "lifts_count" INTEGER NOT NULL,
    "floors_count" INTEGER NOT NULL,
    "construction_year" INTEGER NOT NULL,
    "door_phone" BOOLEAN NOT NULL DEFAULT false,
    "concierge" BOOLEAN NOT NULL DEFAULT false,
    "close_area" BOOLEAN NOT NULL DEFAULT false,
    "code_door" BOOLEAN NOT NULL DEFAULT false,
    "parking" TEXT NOT NULL,
    "landscaping" TEXT NOT NULL,
    "infrastruktura" TEXT NOT NULL,
    "gas" BOOLEAN NOT NULL,
    "garbage_chute" BOOLEAN NOT NULL,
    "years_in_ownership" INTEGER NOT NULL,
    "owner_id" INTEGER NOT NULL,
    "spelled_out_man_count" INTEGER NOT NULL,
    "have_a_buyer" BOOLEAN NOT NULL DEFAULT false,
    "bargaining_is_appropriate" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "real_estate_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "deal" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,
    "realEstateId" INTEGER NOT NULL,

    CONSTRAINT "deal_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "deal_status" (
    "id" SERIAL NOT NULL,
    "status_id" INTEGER NOT NULL,
    "status_title" VARCHAR(15) NOT NULL,
    "status_description" TEXT,
    "CreatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "deal_status_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_real_estateTouser" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_dealTouser" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "deal_realEstateId_key" ON "deal"("realEstateId");

-- CreateIndex
CREATE UNIQUE INDEX "_real_estateTouser_AB_unique" ON "_real_estateTouser"("A", "B");

-- CreateIndex
CREATE INDEX "_real_estateTouser_B_index" ON "_real_estateTouser"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_dealTouser_AB_unique" ON "_dealTouser"("A", "B");

-- CreateIndex
CREATE INDEX "_dealTouser_B_index" ON "_dealTouser"("B");

-- AddForeignKey
ALTER TABLE "deal" ADD CONSTRAINT "deal_realEstateId_fkey" FOREIGN KEY ("realEstateId") REFERENCES "real_estate"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_real_estateTouser" ADD CONSTRAINT "_real_estateTouser_A_fkey" FOREIGN KEY ("A") REFERENCES "real_estate"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_real_estateTouser" ADD CONSTRAINT "_real_estateTouser_B_fkey" FOREIGN KEY ("B") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_dealTouser" ADD CONSTRAINT "_dealTouser_A_fkey" FOREIGN KEY ("A") REFERENCES "deal"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_dealTouser" ADD CONSTRAINT "_dealTouser_B_fkey" FOREIGN KEY ("B") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE;
