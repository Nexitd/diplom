import { ReactNode } from "react";
import { Sidebar } from "widgets/sidebar";

export const PrivateWrapper = ({ children }: { children: ReactNode }) => {
    return <div className="wrapper-bg wrapper-bar-container">
        <Sidebar />
        <div className="wrapper-bar-content">
            {children}
        </div>
    </div>
}