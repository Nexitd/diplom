/*
  Warnings:

  - You are about to drop the column `status_id` on the `deal_status` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[dealId]` on the table `deal_status` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `dealId` to the `deal_status` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "deal_status" DROP COLUMN "status_id",
ADD COLUMN     "dealId" INTEGER NOT NULL;

-- CreateTable
CREATE TABLE "deal_documents_category" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,

    CONSTRAINT "deal_documents_category_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "deal_document" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "categoryId" INTEGER NOT NULL,
    "isChecked" BOOLEAN NOT NULL DEFAULT false,
    "dealId" INTEGER NOT NULL,
    "CreatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "deal_document_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "notification" (
    "id" SERIAL NOT NULL,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "isRead" BOOLEAN NOT NULL DEFAULT false,
    "CreatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "notification_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "favorite" (
    "id" SERIAL NOT NULL,
    "realEstateId" INTEGER NOT NULL,

    CONSTRAINT "favorite_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "favorite_realEstateId_key" ON "favorite"("realEstateId");

-- CreateIndex
CREATE UNIQUE INDEX "deal_status_dealId_key" ON "deal_status"("dealId");

-- AddForeignKey
ALTER TABLE "deal_status" ADD CONSTRAINT "deal_status_dealId_fkey" FOREIGN KEY ("dealId") REFERENCES "deal"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "deal_document" ADD CONSTRAINT "deal_document_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "deal_documents_category"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "deal_document" ADD CONSTRAINT "deal_document_dealId_fkey" FOREIGN KEY ("dealId") REFERENCES "deal"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "favorite" ADD CONSTRAINT "favorite_realEstateId_fkey" FOREIGN KEY ("realEstateId") REFERENCES "real_estate"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
