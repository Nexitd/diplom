"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RealEstatesController = void 0;
const common_1 = require("@nestjs/common");
const real_estates_service_1 = require("./real_estates.service");
const create_real_estate_dto_1 = require("./dto/create-real_estate.dto");
const update_real_estate_dto_1 = require("./dto/update-real_estate.dto");
let RealEstatesController = class RealEstatesController {
    constructor(realEstatesService) {
        this.realEstatesService = realEstatesService;
    }
    create(createRealEstateDto) {
        return this.realEstatesService.create(createRealEstateDto);
    }
    findAll() {
        return this.realEstatesService.findAll();
    }
    findOne(id) {
        return this.realEstatesService.findOne(+id);
    }
    update(id, updateRealEstateDto) {
        return this.realEstatesService.update(+id, updateRealEstateDto);
    }
    remove(id) {
        return this.realEstatesService.remove(+id);
    }
};
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_real_estate_dto_1.CreateRealEstateDto]),
    __metadata("design:returntype", void 0)
], RealEstatesController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], RealEstatesController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], RealEstatesController.prototype, "findOne", null);
__decorate([
    (0, common_1.Patch)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_real_estate_dto_1.UpdateRealEstateDto]),
    __metadata("design:returntype", void 0)
], RealEstatesController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], RealEstatesController.prototype, "remove", null);
RealEstatesController = __decorate([
    (0, common_1.Controller)('real-estates'),
    __metadata("design:paramtypes", [typeof (_a = typeof real_estates_service_1.RealEstatesService !== "undefined" && real_estates_service_1.RealEstatesService) === "function" ? _a : Object])
], RealEstatesController);
exports.RealEstatesController = RealEstatesController;
//# sourceMappingURL=real_estates.controller.js.map