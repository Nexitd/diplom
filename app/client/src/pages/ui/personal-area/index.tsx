import { PersonalAreaForm } from "entities/user";
import { PrivateWrapper } from "../../lib/private-wrapper";

const PersonalArea = () => {
    return <div className="personal wrapper__container">
        <PrivateWrapper>
            <h1 className="personal__area-title">Личные данные</h1>

            <PersonalAreaForm />
        </PrivateWrapper>
    </div>
}

export default PersonalArea;