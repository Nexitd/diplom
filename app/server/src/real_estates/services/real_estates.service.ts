import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateRealEstateDto } from '../dto/create-real_estate.dto';
import { UpdateRealEstateDto } from '../dto/update-real_estate.dto';
import { real_estate } from '@prisma/client';


@Injectable()
export class RealEstatesService {
  constructor(
    private readonly prisma: PrismaService,
  ) {}

  async create(realEstate: CreateRealEstateDto): Promise<real_estate> {
    const newObject = await this.prisma.real_estate.create({
      data: { ...realEstate },
    });

    return newObject;
  }

  async findAllObjects(): Promise<real_estate[]> {
    const objects = this.prisma.real_estate.findMany({});

    return objects;
  }

  async findOne(id: number): Promise<real_estate | {}> {
    const object = await this.prisma.real_estate.findUnique({
      where: { id: id },
    });

    if (!object) throw new BadRequestException();

    return object;
  }

  update(id: number, updateRealEstateDto: UpdateRealEstateDto) {
    return `This action updates a #${id} realEstate`;
  }

  async remove(id: number): Promise<any> {
    await this.prisma.real_estate.delete({ where: { id: id } });

    return { success: true };
  }
}
