import { Action, combineReducers, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { AuthModel } from 'entities/auth';
import { UserModel } from 'entities/user';

const rootReducer = combineReducers({
    auth: AuthModel.authSlice.reducer,
    [AuthModel.authApi.reducerPath]: AuthModel.authApi.reducer,
    [UserModel.userApi.reducerPath]: UserModel.userApi.reducer,
});

export const setupStore = () => {
    return configureStore({
        reducer: rootReducer,
        middleware: (getDefaultMiddleware) =>
            getDefaultMiddleware().concat(
                AuthModel.authApi.middleware,
                UserModel.userApi.middleware,
            ),
    });
};

const store = setupStore();

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;

export default store;
