-- CreateTable
CREATE TABLE "company" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "inn" TEXT NOT NULL,
    "realEstateId" INTEGER NOT NULL DEFAULT 0,
    "CreatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "company_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "images" (
    "id" SERIAL NOT NULL,
    "real_estate_id" INTEGER NOT NULL DEFAULT 0,
    "user_id" INTEGER DEFAULT 0,
    "company_id" INTEGER DEFAULT 0,
    "link" TEXT NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "images_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_companyTouser" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_companyToreal_estate" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "images_real_estate_id_key" ON "images"("real_estate_id");

-- CreateIndex
CREATE UNIQUE INDEX "images_user_id_key" ON "images"("user_id");

-- CreateIndex
CREATE UNIQUE INDEX "images_company_id_key" ON "images"("company_id");

-- CreateIndex
CREATE UNIQUE INDEX "images_link_key" ON "images"("link");

-- CreateIndex
CREATE UNIQUE INDEX "_companyTouser_AB_unique" ON "_companyTouser"("A", "B");

-- CreateIndex
CREATE INDEX "_companyTouser_B_index" ON "_companyTouser"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_companyToreal_estate_AB_unique" ON "_companyToreal_estate"("A", "B");

-- CreateIndex
CREATE INDEX "_companyToreal_estate_B_index" ON "_companyToreal_estate"("B");

-- AddForeignKey
ALTER TABLE "images" ADD CONSTRAINT "images_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "images" ADD CONSTRAINT "images_real_estate_id_fkey" FOREIGN KEY ("real_estate_id") REFERENCES "real_estate"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "images" ADD CONSTRAINT "images_company_id_fkey" FOREIGN KEY ("company_id") REFERENCES "company"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_companyTouser" ADD CONSTRAINT "_companyTouser_A_fkey" FOREIGN KEY ("A") REFERENCES "company"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_companyTouser" ADD CONSTRAINT "_companyTouser_B_fkey" FOREIGN KEY ("B") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_companyToreal_estate" ADD CONSTRAINT "_companyToreal_estate_A_fkey" FOREIGN KEY ("A") REFERENCES "company"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_companyToreal_estate" ADD CONSTRAINT "_companyToreal_estate_B_fkey" FOREIGN KEY ("B") REFERENCES "real_estate"("id") ON DELETE CASCADE ON UPDATE CASCADE;
