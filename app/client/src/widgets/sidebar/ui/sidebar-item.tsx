import { ReactNode, useEffect, useRef, useState } from "react"
import { NavLink } from "react-router-dom";
import cn from "classnames"
import { SidebarArrowIcon } from "shared/icons";

type SidebarOptionType = {
    id: number;
    label: string;
}

type SidebarItemDataProps = {
    id: number;
    path: string;
    label: string;
    icon: ReactNode;
    options: SidebarOptionType[] | []
}

export const SidebarItem = ({ data }: { data: SidebarItemDataProps }) => {
    const [isOpen, setIsOpen] = useState<boolean>(false)
    const [height, setHeight] = useState<number | undefined>(0)
    const collapseBodyRef = useRef<HTMLUListElement>(null);


    useEffect(() => {
        if (!height || !isOpen || !collapseBodyRef.current) return undefined;

        const resizeObserver = new ResizeObserver((el) => {
            setHeight(el[0].contentRect.height);
        });

        resizeObserver.observe(collapseBodyRef.current);

        return () => {
            resizeObserver.disconnect();
        };
    }, [isOpen])

    useEffect(() => {

        if (isOpen) setHeight(collapseBodyRef.current?.getBoundingClientRect().height);

        else setHeight(0);
    }, [isOpen]);

    return <div className={cn("sidebar__item", isOpen && 'sidebar__item-active')}>
        {data.options.length === 0 ?
            <NavLink to={data.path} className={cn("sidebar__item_head")} >
                <div className="sidebar__item_head-flex">
                    {data.icon}
                    <p className="sidebar__item_label">{data.label}</p>
                </div>
                {data.options.length !== 0 && <button className="sidebar__item_btn"><SidebarArrowIcon /></button>}
            </NavLink>
            : <div className="sidebar__item_head">
                <div className="sidebar__item_head-flex">
                    {data.icon}
                    <p className="sidebar__item_label">{data.label}</p>
                </div>
                {data.options.length !== 0 && <button className="sidebar__item_btn" onClick={() => setIsOpen(prev => !prev)}><SidebarArrowIcon /></button>}
            </div>
        }

        <div className="sidebar__item_body" style={{ height }}>
            <ul className="sidebar__item_list" ref={collapseBodyRef}>
                {data.options.map(el => (
                    <li className="sidebar__item_list-item" key={el.id}>
                        <span>{el.label}</span>
                    </li>
                ))}
            </ul>
        </div>
    </div>
}

