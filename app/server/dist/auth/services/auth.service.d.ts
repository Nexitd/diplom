import { JwtService } from '@nestjs/jwt';
import { user as User } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { LoginUserDto } from 'src/users/dto/login-user.dto';
import { UserDto } from '../dto/user.dto';
export declare class AuthService {
    private prisma;
    private jwtService;
    constructor(prisma: PrismaService, jwtService: JwtService);
    generateToken(user: UserDto): Promise<{
        status: number;
        token: string;
    }>;
    validateUser(user: LoginUserDto): Promise<{
        id: number;
        email: string;
        name: string;
        surname: string;
        phone: string;
        patronymic: string;
        birth_date: Date;
        CreatedAt: Date;
        UpdatedAt: Date;
        user_roles: any;
    }>;
    login(userData: LoginUserDto): Promise<{
        status: number;
        token: string;
    }>;
    registerUser(user: CreateUserDto): Promise<User>;
    findByEmail(email: string): Promise<User & {
        user_roles: any;
    }>;
    findById(id: number): Promise<Omit<User, 'password'>>;
    getUserRoles(userId: number): Promise<(import(".prisma/client").user_roles & {
        role: {
            name: string;
        };
    })[]>;
}
