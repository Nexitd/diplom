import { Injectable } from '@nestjs/common';
import { PassportSerializer } from '@nestjs/passport';
import { AuthService } from '../services/auth.service';
import { user as User } from '@prisma/client';

@Injectable()
export class AuthSerializer extends PassportSerializer {
  constructor(private readonly authService: AuthService) {
    super();
  }

  serializeUser(user: User, done: (err: Error, user: { id: number }) => void) {
    done(null, { id: user.id });
  }

  async deserializeUser(
    payload: { id: number },
    done: (
      err: Error,
      user: Omit<User, 'password' | 'confirmationCode'>,
    ) => void,
  ) {
    const user = await this.authService.findById(payload.id);
    
    done(null, user);
  }
}
