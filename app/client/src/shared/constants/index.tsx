// app roles 


// filters for main page quick filtration
import { selectItemsType } from "shared/types";

export const sectorsItems: selectItemsType[] = [
    {
        label: "Ворошиловский",
        value: 'voroshilovskiy',
    },
    {
        label: "Первомайский",
        value: 'pervomayskiy',
    },
    {
        label: "Октябрьский",
        value: 'oktyabrskiy',
    },
    {
        label: "Ленинский",
        value: 'leninskiy',
    },
    {
        label: "Кировский",
        value: 'kirovskiy',
    },
    {
        label: "Пролетарский",
        value: 'proletarskiy',
    },
    {
        label: "Советский",
        value: 'sovetskiy',
    },
    {
        label: "Железнодорожный",
        value: 'zheleznodorozhniy',
    },
];

export const roomsTypeItems: selectItemsType[] = [
    {
        label: "квартира в новостройке",
        value: 'novostroyka',
    },
    {
        label: "квартира во вторичке",
        value: 'vtorichka',
    },
];

export const roomsCountItems: selectItemsType[] = [
    {
        label: "студия",
        value: 'studia',
    },
    {
        label: "1",
        value: '1',
    },
    {
        label: "2",
        value: '2',
    },
    {
        label: "3",
        value: '3',
    },
    {
        label: "4",
        value: '4',
    },
];


export const priceTypeItems: selectItemsType[] = [
    {
        label: "0 ₽ - 900 000 ₽",
        value: '0-900000',
    },
    {
        label: "900 000 ₽ - 3 000 000 ₽",
        value: '900000-3000000',
    },
    {
        label: "3 000 000 ₽ - 6 000 000 ₽",
        value: '3000000-6000000',
    },
    {
        label: "6 000 000 ₽ - 12 000 000 ₽",
        value: '6000000-12000000',
    },
    {
        label: "12 000 000 ₽ и более",
        value: '12000000',
    },
];


// footer & header actions

export const actions = [
    {
        id: 'catalog' as const,
        label: 'Каталог',
        url: '/catalog',
    },
    {
        id: 'buyers' as const,
        label: 'Покупателям',
        url: '/buyers',
    },
    {
        id: 'sellers' as const,
        label: 'Продавцам',
        url: '/sellers',
    },
    {
        id: 'realtorpam' as const,
        label: 'Риэлторам',
        url: '/realtorpam',
    },
    {
        id: 'lawyers' as const,
        label: 'Юристам',
        url: '/lawyers',
    },
    {
        id: 'contacts' as const,
        label: 'Контакты',
        url: '/contacts',
    },
];

