import { Module } from '@nestjs/common';
import { RealEstatesService } from './services/real_estates.service';
import { RealEstatesController } from './controllers/real_estates.controller';
import { PrismaModule } from 'src/prisma/prisma.module';

@Module({
  controllers: [RealEstatesController],
  providers: [RealEstatesService],
  imports: [PrismaModule],
})
export class RealEstatesModule {}
