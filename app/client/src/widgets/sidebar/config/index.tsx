import { AddToDealIcon, ChatIcon, CompanyEmplyeesIcon, FavouriteIcon, MyDealsIcon, MyEmployeesIcon, NotificationIcon, PersonalDataIcon, SailObjcetsIcon } from "shared/icons";

const userItems = [
    {
        id: 1,
        path: '/my-deals',
        label: 'Мои сделки',
        icon: <MyDealsIcon />,
        options: [
            {
                id: 1,
                label: "Все",
            },
            {
                id: 2,
                label: "Активные",
            },
            {
                id: 3,
                label: "Ожидаемые",
            },
            {
                id: 4,
                label: "Завершённые",
            },
            {
                id: 5,
                label: "Отмененные",
            },
        ]
    },
    {
        id: 2,
        path: '/sail-objects',
        label: 'Объекты на продажу',
        icon: <SailObjcetsIcon />,
        options: []
    },
    {
        id: 3,
        path: '/notifications',
        label: 'Уведомления',
        icon: <NotificationIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Новые"
            },
            {
                id: 3,
                label: "Прочитанные"
            },
        ]
    },
    {
        id: 4,
        path: '/chats',
        label: 'Чаты',
        icon: <ChatIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Риелторы"
            },
            {
                id: 3,
                label: "Юристы"
            },
            {
                id: 4,
                label: "Продавцы"
            },
        ]
    },
    {
        id: 5,
        path: '/favourite',
        label: 'Избранное',
        icon: <FavouriteIcon />,
        options: []
    },
    {
        id: 6,
        path: '/personal-area',
        label: 'Личные данные',
        icon: <PersonalDataIcon />,
        options: []
    },
];

const directorItems = [
    {
        id: 1,
        path: "/my-employees",
        label: "Мои сотрудники",
        icon: <MyEmployeesIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Риелторы"
            },
            {
                id: 3,
                label: "Юристы"
            },
            {
                id: 4,
                label: "Менеджеры"
            },
        ]
    },
    {
        id: 2,
        path: '/notifications',
        label: 'Уведомления',
        icon: <NotificationIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Новые"
            },
            {
                id: 3,
                label: "Прочитанные"
            },
        ]
    },
    {
        id: 3,
        path: '/chats',
        label: 'Чаты',
        icon: <ChatIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Риелторы"
            },
            {
                id: 3,
                label: "Юристы"
            },
            {
                id: 4,
                label: "Менедджеры"
            },
        ]
    },
    {
        id: 4,
        path: '/personal-area',
        label: 'Личные данные',
        icon: <PersonalDataIcon />,
        options: []
    },
];

const moderItems = [
    {
        id: 1,
        path: "/employees",
        label: 'Специалисты компании',
        icon: <CompanyEmplyeesIcon />,
        options: [
            {
                id: 1,
                label: "Все",
            },
            {
                id: 2,
                label: "Риелторы",
            },
            {
                id: 3,
                label: "Юристы",
            },
            {
                id: 4,
                label: "Общая статистика",
            },
        ]
    },
    {
        id: 2,
        path: "/deals/add-employee",
        label: 'Сделки и назначение сотрудников',
        icon: <AddToDealIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Ожидаемые"
            },
            {
                id: 3,
                label: "Активные"
            },
            {
                id: 4,
                label: "Завершённые"
            },
            {
                id: 5,
                label: "Отмененные"
            },
        ]
    },
    {
        id: 3,
        path: '/notifications',
        label: 'Уведомления',
        icon: <NotificationIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Новые"
            },
            {
                id: 3,
                label: "Прочитанные"
            },
        ]
    },
    {
        id: 4,
        path: '/chats',
        label: 'Чаты',
        icon: <ChatIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Риелторы"
            },
            {
                id: 3,
                label: "Юристы"
            }
        ]
    },
    {
        id: 5,
        path: '/personal-area',
        label: 'Личные данные',
        icon: <PersonalDataIcon />,
        options: []
    },
];

const realtorItems = [
    {
        id: 1,
        path: '/my-deals',
        label: 'Мои сделки',
        icon: <MyDealsIcon />,
        options: [
            {
                id: 1,
                label: "Все",
            },
            {
                id: 2,
                label: "Активные",
            },
            {
                id: 3,
                label: "Ожидаемые",
            },
            {
                id: 4,
                label: "Завершённые",
            },
            {
                id: 5,
                label: "Отмененные",
            },
        ]
    },
    {
        id: 2,
        path: '/notifications',
        label: 'Уведомления',
        icon: <NotificationIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Новые"
            },
            {
                id: 3,
                label: "Прочитанные"
            },
        ]
    },
    {
        id: 3,
        path: '/chats',
        label: 'Чаты',
        icon: <ChatIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Риелторы"
            },
            {
                id: 3,
                label: "Юристы"
            },
            {
                id: 4,
                label: "Собственники"
            },
        ]
    },
    {
        id: 4,
        path: '/personal-area',
        label: 'Личные данные',
        icon: <PersonalDataIcon />,
        options: []
    },

]

const lawyerItems = [
    {
        id: 1,
        path: '/my-deals',
        label: 'Мои сделки',
        icon: <MyDealsIcon />,
        options: [
            {
                id: 1,
                label: "Все",
            },
            {
                id: 2,
                label: "Активные",
            },
            {
                id: 3,
                label: "Ожидаемые",
            },
            {
                id: 4,
                label: "Завершённые",
            },
            {
                id: 5,
                label: "Отмененные",
            },
        ]
    },
    {
        id: 2,
        path: '/notifications',
        label: 'Уведомления',
        icon: <NotificationIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Новые"
            },
            {
                id: 3,
                label: "Прочитанные"
            },
        ]
    },
    {
        id: 3,
        path: '/chats',
        label: 'Чаты',
        icon: <ChatIcon />,
        options: [
            {
                id: 1,
                label: "Все"
            },
            {
                id: 2,
                label: "Риелторы"
            },
            {
                id: 3,
                label: "Юристы"
            },
            {
                id: 4,
                label: "Собственники"
            },
        ]
    },
    {
        id: 4,
        path: '/personal-area',
        label: 'Личные данные',
        icon: <PersonalDataIcon />,
        options: []
    },
];

export const sidebarData = {
    "USER": userItems,
    "DIRECTOR": directorItems,
    "LAWYER": lawyerItems,
    "REALTOR": realtorItems,
    "MODER": moderItems
}