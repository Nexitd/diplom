import { Response } from 'express';
export declare class AppController {
    constructor();
    seeUploadedFile(image: string, res: Response): void;
}
