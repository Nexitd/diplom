import { PassportSerializer } from '@nestjs/passport';
import { AuthService } from '../services/auth.service';
import { user as User } from '@prisma/client';
export declare class AuthSerializer extends PassportSerializer {
    private readonly authService;
    constructor(authService: AuthService);
    serializeUser(user: User, done: (err: Error, user: {
        id: number;
    }) => void): void;
    deserializeUser(payload: {
        id: number;
    }, done: (err: Error, user: Omit<User, 'password' | 'confirmationCode'>) => void): Promise<void>;
}
