import { FC } from "react";
import { NavLink } from "react-router-dom";
import logo from "assets/images/Logo 1.svg"
import { actions } from "shared/constants";

const Footer: FC = () => {
    return (
        <div className="footer">
            <div className="footer__container wrapper__container">
                <div className="footer__container_item">
                    <NavLink to="/" className='header__container_logolink'>
                        <img src={logo} alt="logo" />
                    </NavLink>
                </div>
                <div className="footer__container_item">
                    <ul className="footer__list">
                        {actions.map(({ id, label, url }) => (
                            <NavLink
                                key={id}
                                to={url}
                                className='header__container_link'
                            >
                                <span>{label}</span>
                            </NavLink>
                        ))}

                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Footer;