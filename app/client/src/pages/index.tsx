import { Routes } from "react-router-dom";
import { routes } from "./lib/routes";
import { CreateRoute } from "./lib/create-route";


export const Routing = () => {
    return (
        <Routes>
            {routes.map(CreateRoute)}
        </Routes>
    );
};