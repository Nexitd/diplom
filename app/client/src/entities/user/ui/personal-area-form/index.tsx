import { useEffect } from "react"
import { Form, Input } from "antd"
import { UploadFile } from "features/upload-file"
import { UserModel } from "entities/user"
import { Button, CustomInput } from "shared/ui"
import { parseJwt } from "shared/api"


export const PersonalAreaForm = () => {
    const [form] = Form.useForm();
    const { id } = parseJwt(localStorage.getItem("access_token"))
    const { data, isFetching } = UserModel.useGetMeQuery(id!)
    const [addImage] = UserModel.useAddUserImagesMutation()
    const [deleteImage] = UserModel.useDeleteUserImageMutation()
    const [updateUsersData] = UserModel.useUpdateUsersDataMutation()

    useEffect(() => {
        if (!isFetching) {
            form.setFieldsValue(data)
        }
    }, [isFetching])

    const onFinish = async (values: any) => {
        delete values.repeat_password;

        const form_data = new FormData();

        if (values.image) {
            form_data.append("file", values.image.file);

            if (data?.images) {
                await deleteImage(data!.images!.id);
            }

            await addImage({ user_id: id, file: form_data })
        }

        delete values.image;

        await updateUsersData(values)
    }

    return <div className="personal__form">
        <Form className="form" autoComplete="off" form={form} onFinish={(values) => onFinish(values)}>
            <UploadFile link={data?.images !== null ? data?.images.link : ''} />
            <div className="personal__form_container">
                <div className="personal__form_item">
                    <h2 className="personal__form_title">Контактные данные</h2>
                    <CustomInput className="form_item" name="surname" rules={[
                        {
                            required: true,
                            message: "Поле обязательно для заполнения!"
                        }
                    ]} />
                    <CustomInput className="form_item" name="name" rules={[
                        {
                            required: true,
                            message: "Поле обязательно для заполнения!"
                        }
                    ]} />
                    <CustomInput className="form_item" name="patronymic" rules={[
                        {
                            required: true,
                            message: "Поле обязательно для заполнения!"
                        }
                    ]} />
                    <CustomInput className="form_item" name="email" rules={[
                        {
                            required: true,
                            message: "Поле обязательно для заполнения!"
                        },
                        {
                            type: "email",
                            message: "Не валидное значение!"
                        }
                    ]} />
                    <CustomInput className="form_item" type="tel" name="phone" rules={[
                        {
                            required: true,
                            message: "Поле обязательно для заполнения!"
                        }
                    ]} />

                    <div className="personal__form_btns">
                        <Button type="button" className="btn-ghost" text="Отменить" />
                        <Button type="submit" text="Сохранить" />
                    </div>
                </div>
                <div className="personal__form_item">
                    <h2 className="personal__form_title">Изменить пароль</h2>
                    <Form.Item className="form_item" name="old_password" rules={[
                        {
                            required: true,
                            message: "Поле обязательно для заполнения!"
                        }
                    ]}>
                        <Input.Password placeholder="Старый пароль" />
                    </Form.Item>
                    <Form.Item className="form_item" name="new_password" rules={[
                        {
                            required: true,
                            message: "Поле обязательно для заполнения!"
                        }
                    ]}>
                        <Input.Password placeholder="Новый пароль" />
                    </Form.Item>
                    <Form.Item className="form_item" name="repeat_password" dependencies={['new_password']} rules={[
                        {
                            required: true,
                            message: "Поле обязательно для заполнения!"
                        }, ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('new_password') === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('Пароли не совпадают!'));
                            },
                        }),
                    ]}>
                        <Input.Password placeholder="Повторите пароль" />
                    </Form.Item>
                </div>
            </div>
        </Form>
    </div>
}