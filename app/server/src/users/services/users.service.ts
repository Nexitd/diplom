import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma/prisma.service';
import { UpdateUserDto } from '../dto/update-user.dto';
import { user } from '@prisma/client';

@Injectable()
export class UsersService {
  constructor(private readonly prisma: PrismaService) {}

  async findAllUsers(): Promise<Omit<user, 'password'>[]> {
    const users = await this.prisma.user.findMany({
      include: {
        user_roles: {
          select: {
            role: true,
          },
        },
      },
    });

    const usersWithoutPassword = users.map((el) =>
      this.excludePassword(el, ['password']),
    );

    return usersWithoutPassword;
  }

  excludePassword<User, Key extends keyof User>(
    user: User,
    keys: Key[],
  ): Omit<User, Key> {
    for (let key of keys) {
      delete user[key];
    }

    return user;
  }

  async findOneUser(id: number): Promise<Omit<user, 'password'> | null> {
    const user = await this.prisma.user.findUnique({
      where: { id: id },
      include: {
        user_roles: {
          select: {
            role: true,
          },
        },
        images: {
          select: {
            id: true,
            link: true,
          },
        },
      },
    });

    const userWithoutPassword = this.excludePassword(user, ['password']);

    return userWithoutPassword;
  }

  async updateUser(id: number, updateUserDto: UpdateUserDto) {
    
  }

  async removeUser(id: number): Promise<void> {
    await this.prisma.user.delete({ where: { id: id } });
  }
}
