import { roles } from 'shared/types';

type JwtParseDataType = {
    id: string | null;
    role: roles | null;
};

export const parseJwt = (token: string | null = ''): JwtParseDataType => {
    try {
        if (token !== null) {
            return {
                id: JSON.parse(atob(token.split('.')[1])).id,
                role: JSON.parse(atob(token.split('.')[1])).role,
            };
        }

        return {
            id: null,
            role: null,
        };
    } catch (e) {
        return {
            id: null,
            role: null,
        };
    }
};
